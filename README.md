# Dashboard
Reference, de-branded implementation of a user dashboard [PWA](https://web.dev/progressive-web-apps/) for a Safe Surfer Core deployment. Written with Vue 2 + Typescript. This is designed to be an easy starting point for theming your own version, or as an example of how to use the APIs. 

This version of the dashboard only includes parts that you're likely to use for a re-deployment. If there's anything you're missing from [my.safesurfer.io](https://my.safesurfer.io), you can see the fork for that site [here](https://gitlab.com/safesurfer/my).

## Branding Guide
You can change the colors from `src/colors.scss`. You can set any of the [Bulma variables](https://bulma.io/documentation/customize/variables/) from the stylesheet in `src/App.vue`. You will also want to update `public/favicon.ico` and the contents of `public/img` with your own favicon.

Find/replace the following strings in the repo to start branding. Review each one to make sure it makes sense gramatically.
- `BRAND_NAME`, e.g. "Safe Surfer".
- `ROUTER_NAME`. The name of the "smart router" implementation, which refers to any device running the Safe Surfer router integration. For example, we market this as the "lifeguard" device. You can generally substitute this for just "router" or "smart router". Many internal pieces still refer to this as "lifeguard", but these are not shown to the user.
- `PLAN_NAME`. When restricting into a paid/free tier, what to name the paid tier.
- `CONTACT_URL`
- `PRIVACY_POLICY_URL`
- `TERMS_AND_CONDITIONS_URL`
- `BRAND_NAME_KEBAB_CASE`, e.g. `safe-surfer`, used in some download file names.
- `DOMAIN_NAME_FILTER` should filter for any domain where the dashboard is being hosted, including the API, to prevent users from blocking it.
- `HELP_URL`, a URL that should point to a help page. E.g. in our case, it points to our [helpdesk](https://helpdesk.safesurfer.io).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Building
Just tag the gitlab repo to automatically build an image into the repo's container registry. You can then use the container image URL in the values file of your Safe Surfer Core deployment.

```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
