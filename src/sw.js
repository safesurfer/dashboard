import {precacheAndRoute} from 'workbox-precaching';

precacheAndRoute(self.__WB_MANIFEST);

self.addEventListener('push', function(event) {
  const notifObj = JSON.parse(event.data.text());
  event.waitUntil(
    registration.showNotification('BRAND_NAME', {
      body: notifObj.text,
      data: notifObj.data
    })
  )
})

self.addEventListener('notificationclick', function(event) {
  const notification = event.notification;
  notification.close();
  const link = notification.data.link
  event.waitUntil(clients.openWindow(link));
});

self.addEventListener('activate', function(event) {
  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        cacheNames.filter(function(cacheName) {
          // Return true if you want to remove this cache,
          // but remember that caches are shared across
          // the whole origin
        }).map(function(cacheName) {
          return caches.delete(cacheName);
        })
      );
    })
  );
});

self.addEventListener('message', data => {
  skipWaiting()
})