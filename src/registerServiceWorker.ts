/* eslint-disable no-console */

import { Workbox } from 'workbox-window'
import { SnackbarProgrammatic } from 'buefy'

let refresh = false

if ('serviceWorker' in navigator && localStorage.getItem('my-ss-immersive') !== '1') {
  const wb = new Workbox('sw.js');
 
  wb.addEventListener('installed', event => {
    if (event.isUpdate) {
      SnackbarProgrammatic.open({
        message: 'Dashboard update available!',
        type: 'is-primary',
        actionText: 'Refresh',
        duration: 10000,
        onAction: () => {
          event.sw?.postMessage('skipWaiting')
          refresh = true
        }
      })
    }
  });

  wb.addEventListener('activated', () => {
    if (refresh) {
      window.location.reload();
    }
  })

  wb.register();
}
