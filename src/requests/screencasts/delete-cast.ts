import Request from "../request";

export default class DeleteCast extends Request<null, number> {
  handle(opts: number): Promise<null> {
    return this.authReq({
      url: '/screencasts/cast/' + opts,
      method: 'DELETE'
    })
  }
}
