import Request from '@/requests/request'

export interface Casts {
  /**
   * The actual casts.
   */
  casts: Cast[];
  /**
   * How many casts are available, including the requested page.
   */
  available: number;
}

export interface Cast {
  /**
   * ID of the cast.
   */
  id: number;
  /**
   * Unix timestamp of when the cast began.
   */
  startTimestamp: number;
  /**
   * Unix timestamp of when the cast ended. May be 0, meaning it hasn't ended yet.
   */
  endTimestamp: number;
  /**
   * ID of the device that is casting
   */
  deviceID: string;
  /**
   * Amount of events recorded during this cast.
   */
  numEvents: number;
  /**
   * Unix timestamp of when the last event occurred. Will be 0 if no event has occurred.
   */
  lastEventTimestamp: number;
}

export interface GetCastsOptions {
  page: number;
  'num-per-page': number;
  /**
   * Device ID to request, or "all" for any device.
   */
  device: string;
}

export default class GetCasts extends Request<Casts, GetCastsOptions> {
  /**
   * @returns The casts for the requesting page, 
   * @param opts Query options.
   */
  handle (opts: GetCastsOptions): Promise<Casts> {
    return this.authReq({
      url: '/screencasts',
      method: 'GET',
      params: opts
    })
  }
}
