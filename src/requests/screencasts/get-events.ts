import Request from '@/requests/request'

export interface APICastEvents {
  /**
   * The actual events.
   */
  events: APICastEvent[] | null;
  /**
   * How many events are available for the request total, including current page.
   */
  available: number;
}

export interface APICastEvent {
  /**
   * Unique ID of the event.
   */
  id: number;
  /**
   * Unix timestamp of when the event was recorded, not when it was received
   */
  timestamp: number;
  /**
   * Map from category to outputs.
   */
  levels: {[catID: string]: number};
  /**
   * Confidence in the prediction.
   */
  confidence: 'LOW' | 'MEDIUM' | 'HIGH';
  /**
   * Whether there is an associated image.
   */
  image: boolean;
  /**
   * Device ID that recorded the event.
   */
  deviceID: string;
  /**
   * Category of the event.
   */
  category: string;
}

export interface GetCastEventsOptions {
  /**
   * The screencast ID to get events for.
   */
  castID: number;
  /**
   * Which page to request.
   */
  page: number;
  /**
   * How many events to show on each page.
   */
  'num-per-page': number;
}

export default class GetEvents extends Request<APICastEvents, GetCastEventsOptions> {
  /**
   * @returns The cast events for a particular page and device.
   * @param opts Request params.
   */
  handle (opts: GetCastEventsOptions): Promise<APICastEvents> {
    return this.authReq({
      url: '/screencasts/cast/' + opts.castID + '/events',
      method: 'GET',
      params: {
        page: opts.page,
        'num-per-page': opts['num-per-page']
      }
    })
  }
}
