import Request from '@/requests/request'

export interface GetEventImageOptions {
  /**
   * The ID of the cast the event comes from.
   */
  castID: number;
  /**
   * The ID of the event to get the image for.
   */
  eventID: number;
}

export default class GetEventImage extends Request<Blob, GetEventImageOptions> {
  /**
   * @returns The image for a screencast event as a blob.
   * @param opts The cast and event IDs.
   */
  handle (opts: GetEventImageOptions): Promise<Blob> {
    return this.authReq({
      url: '/screencasts/cast/' + opts.castID + '/events/event/' + opts.eventID + '/image',
      responseType: 'blob'
    })
  }
}
