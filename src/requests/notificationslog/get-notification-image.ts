import Request from "../request";

export default class GetNotificationImage extends Request<Blob, number> {
  handle(opts: number): Promise<Blob> {
    return this.authReq({
      url: '/v2/notifications-log/notifications/' + opts + '/image',
      responseType: 'blob'
    })
  }
}
