import Request, { MetaResp } from "../request"

export default class DeleteNotification extends Request<MetaResp<null>, number> {
  /**
   * Delete a notification by ID.
   * @param opts The notification ID.
   */
  handle(opts: number): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/notifications-log/notifications/' + opts,
      method: 'DELETE'
    })
  }
}
