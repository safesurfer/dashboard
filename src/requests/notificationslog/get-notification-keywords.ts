import Request, { MetaResp } from "../request"

export default class GetNotificationKeywords extends Request<MetaResp<string[]>, undefined> {
  /**
   * @returns All keywords for the notifications the user has.
   */
  handle(): Promise<MetaResp<string[]>> {
    return this.authReq({
      url: '/v2/notifications-log/notifications/keywords',
      method: 'GET'
    })
  }
}
