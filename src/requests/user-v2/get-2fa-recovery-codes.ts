import Request, { MetaResp } from "../request";

export default class Get2FARecoveryCodes extends Request<MetaResp<string[]>, undefined> {
  handle(): Promise<MetaResp<string[]>> {
    return this.authReq({
      url: '/v2/user/auth/twofactor/recovery/codes',
      method: 'GET'
    })
  }
}