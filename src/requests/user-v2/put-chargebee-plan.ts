import Request, { MetaResp } from "../request";
import { GetPlanArgs } from "./get-plan";

export interface PutChargebeePlanArgs {
  planID: string;
}

export default class PutChargebeePlan extends Request<MetaResp<null>, PutChargebeePlanArgs> {
  handle(opts: PutChargebeePlanArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/user/subscription/chargebee/plan',
      method: 'PUT',
      data: opts
    })
  }

  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
