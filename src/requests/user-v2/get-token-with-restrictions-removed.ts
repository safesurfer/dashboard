import Request, { MetaResp } from "../request";
import { APIAuthResponse } from "./post-auth-token";

export default class GetTokenWithRestrictionsRemoved extends Request<MetaResp<APIAuthResponse>, undefined> {
  handle(): Promise<MetaResp<APIAuthResponse>> {
    return this.authReq({
      url: '/v2/user/auth/token/with-restrictions-removed',
      method: 'GET'
    })
  }
}
