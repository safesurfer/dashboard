import Request, { MetaResp } from "../request";
import { APIAuthResponse } from "./post-auth-token";

export default class Post2FAToken extends Request<MetaResp<APIAuthResponse>, string> {

  private doRedirectOn401 = true

  constructor (redirectOn401: boolean) {
    super()
    this.doRedirectOn401 = redirectOn401
  }

  handle(opts: string): Promise<MetaResp<APIAuthResponse>> {
    return this.authReq({
      url: '/v2/user/auth/twofactor/token',
      method: 'POST',
      data: this.encodeAsForm({
        token: opts
      })
    })
  }

  /**
   * Whether to redirect to the login page for a 401.
   */
   protected redirectOn401 (): boolean {
    return this.doRedirectOn401
  }

  /**
   * Given an axios error and promise reject function, reject
   * the promise. Default behavior to reject with error code.
   * @param err The axios error.
   * @param reject Reject function.
   */
   protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response) {
      reject(err.response)
    } else {
      reject() // eslint-disable-line
    }
  }
}
