import Request, { MetaResp } from "../request";

export default class Post2FARecoveryCodesReset extends Request<MetaResp<null>, undefined> {
  handle(): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/user/auth/twofactor/recovery/codes/reset',
      method: 'POST'
    })
  }
}
