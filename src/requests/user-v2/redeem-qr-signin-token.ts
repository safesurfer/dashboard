import Request, { MetaResp } from "../request";

export interface RedeemQrArgs {
  token: string;
  type: string;
  os: string;
  name: string;
  roles?: string;
}

export interface QrSigninTokenRedemption {
  deviceID: string;
  dnsToken: string;
  authToken: string;
  email: string;
}

export default class RedeemQrSigninToken extends Request<MetaResp<QrSigninTokenRedemption>, RedeemQrArgs> {
  handle(opts: RedeemQrArgs): Promise<MetaResp<QrSigninTokenRedemption>> {
    return this.simpleReq({
      url: '/v2/user/auth/qr-signin-tokens/' + opts.token + '/redeem',
      method: 'POST',
      data: this.encodeAsForm(opts)
    })
  }
}
