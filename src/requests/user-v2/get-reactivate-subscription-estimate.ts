import Request, { MetaResp } from "../request";
import { ChangeSubscriptionEstimate } from "../user-v2/post-subscription-estimate";

export default class GetReactivateSubscriptionEstimate extends Request<MetaResp<ChangeSubscriptionEstimate>, undefined> {
  handle (): Promise<MetaResp<ChangeSubscriptionEstimate>> {
    return this.authReq({
      url: '/v2/user/subscription/reactivate/estimate',
      method: 'GET'
    })
  }

  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
