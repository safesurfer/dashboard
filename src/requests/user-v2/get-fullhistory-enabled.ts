import Request, { MetaResp } from "../request"

export default class GetFullHistoryEnabled extends Request<MetaResp<boolean>, undefined> {
  /**
   * @returns Whether full history is enabled for the current user.
   */
  handle(): Promise<MetaResp<boolean>> {
    return this.authReq({
      url: '/v2/user/fullhistory/enabled',
      method: 'GET'
    })
  }
}
