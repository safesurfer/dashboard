import Request, { MetaResp } from '@/requests/request'

export interface PostUserOptions {
  email: string;
  password: string;
  /**
   * The user ID of the user referring the user signing up.
   */
  referer?: number;
}

export interface UserCreateResult {
  id: string;
  createdEmail: string;
}

export default class PostUser extends Request<MetaResp<UserCreateResult>, PostUserOptions> {
  /**
   * Create an account with the sign in details.
   * @param opts The sign in details.
   */
  handle (opts: PostUserOptions): Promise<MetaResp<UserCreateResult>> {
    return this.simpleReq({
      url: '/v2/user',
      method: 'POST',
      data: this.encodeAsForm(opts)
    })
  }

  /**
   * Given an axios error and promise reject function, reject
   * the promise. Default behavior to reject with error code.
   * @param err The axios error.
   * @param reject Reject function.
   */
  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response) {
      reject(err.response)
    } else {
      reject() // eslint-disable-line
    }
  }
}
