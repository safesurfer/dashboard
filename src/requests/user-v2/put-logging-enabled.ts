import Request, { MetaResp } from '../request'

export default class PutLoggingEnabled extends Request<MetaResp<null>, boolean> {
  /**
   * Set whether logging is enabled for the current user.
   * @param opts Whether to enable logging.
   */
  handle(opts: boolean): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/user/logging/enabled',
      method: 'PUT',
      data: opts
    })
  }
}
