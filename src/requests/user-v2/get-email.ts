import Request, { MetaResp } from "../request";

export default class GetEmail extends Request<MetaResp<string>, undefined> {
  handle(): Promise<MetaResp<string>> {
    return this.authReq({
      url: '/v2/user/email',
      method: 'GET'
    })
  }
}
