import Request, { MetaResp } from "../request";

export default class GetData extends Request<MetaResp<{[key: string]: string}>, undefined> {
  handle(): Promise<MetaResp<{ [key: string]: string; }>> {
    return this.authReq({
      url: '/v2/user/data',
      method: 'GET'
    })
  }
}
