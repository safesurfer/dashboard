import Request, { MetaResp } from "../request";

export interface GetPlanArgs {
  id: string | null;
  'price-id': string | null;
  period: string | null;
  prefix: string | null;
}

export interface PriceAndPeriod {
  price: string;
  period: string;
  currencySymbol: string;
  currencyCode: string;
  numericPrice: number;
}

export interface Trial {
  trial: string;
  trialPeriod: number;
  trialPeriodUnit: string;
}

export interface QuotaConfig {
  maxValue?: number;
  valueDelta?: number;
}

export interface AddonPricingTier {
  start: number;
  end: number;
  price: PriceAndPeriod;
}

export interface AddonPrice {
  pricingModel: string;
  priceAndPeriod: PriceAndPeriod;
  pricingTiers: AddonPricingTier[];
}

export interface Addon {
  id: string;
  price: AddonPrice;
  quotas: {[quotaName: string]: QuotaConfig};
  minQuantity?: number;
  maxQuantity?: number;
  prorateOnUpgrade: boolean;
  prorateOnDowngrade: boolean;
  invoiceImmediately: boolean;
  requireActive: boolean;
}

export interface Plan {
  enabled: boolean;
  id: string;
  priceID: string;
  channel: string;
  price: PriceAndPeriod;
  trial: Trial;
  anyPlanQuotas?: {[quotaName: string]: QuotaConfig};
  anyCbPlanQuotas?: {[quotaName: string]: QuotaConfig};
  planQuotas?: {[quotaName: string]: QuotaConfig};
  addons: Addon[];
}

export default class GetPlan extends Request<MetaResp<Plan>, GetPlanArgs> {
  handle(opts: GetPlanArgs): Promise<MetaResp<Plan>> {
    return this.simpleReq({
      url: '/v2/user/plans/chargebee',
      method: 'GET',
      params: opts
    })
  }
}
