import Request, { MetaResp } from "../request";

export interface CreateChargebeeAddonArgs {
  id: string;
  quantity: number;
}

export interface CreateChargebeeSubscriptionArgs {
  planID: string;
  planPriceID: string;
  planPrefix: string;
  planPeriod: string;
  coupon: string;
  addons: CreateChargebeeAddonArgs[];
}

export interface CreateSubscriptionArgs {
  resubscribe: boolean;
  chargebee: CreateChargebeeSubscriptionArgs;
}

export default class PostSubscription extends Request<MetaResp<null>, CreateSubscriptionArgs> {
  handle(opts: CreateSubscriptionArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/user/subscription',
      method: 'POST',
      data: opts
    })
  }

  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
