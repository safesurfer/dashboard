import Request, { MetaResp } from '@/requests/request'

export default class GetLoggingEnabled extends Request<MetaResp<boolean>, undefined> {
  /**
   * @returns Whether logging is enabled for the current user.
   */
  handle(): Promise<MetaResp<boolean>> {
    return this.authReq({
      url: '/v2/user/logging/enabled',
      method: 'GET'
    })
  }
}
