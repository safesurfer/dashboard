import { getBaseURLIpv6 } from '@/helpers/functions'
import Request, { MetaResp } from '@/requests/request'

export default class GetIP6V2 extends Request<MetaResp<string>, undefined> {
  /**
   * @returns The user's IPv6.
   */
  handle(): Promise<MetaResp<string>> {
    return this.simpleReq({
      url: '/v2/my-ip',
      method: 'GET'
    })
  }

  protected getBaseURL(): string {
    return getBaseURLIpv6()
  }
}
