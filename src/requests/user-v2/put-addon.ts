import Request, { MetaResp } from "../request";
import { CreateChargebeeAddonArgs } from "./post-subscription";

export default class PutAddon extends Request<MetaResp<null>, CreateChargebeeAddonArgs> {
  handle (opts: CreateChargebeeAddonArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/user/subscription/addons/' + opts.id,
      method: 'PUT',
      data: opts
    })
  }

  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
