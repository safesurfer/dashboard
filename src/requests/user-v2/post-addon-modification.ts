import Request, { MetaResp } from "../request";

export interface AddonChange {
  id: string;
  delete: boolean;
  newQuantity: number | null;
}

export interface AddonModification {
  changes: AddonChange[];
}

export default class PostAddonModification extends Request<MetaResp<null>, AddonModification> {
  handle(opts: AddonModification): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/user/subscription/addons/modify',
      method: 'POST',
      data: opts
    })
  }

  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
