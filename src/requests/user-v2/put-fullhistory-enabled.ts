import Request, { MetaResp } from '../request'

export default class PutFullHistoryEnabled extends Request<MetaResp<null>, boolean> {
  /**
   * Set whether full history is enabled for the current user.
   * @param opts Whether to enable full history.
   */
  handle(opts: boolean): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/user/fullhistory/enabled',
      method: 'PUT',
      data: opts
    })
  }
}
