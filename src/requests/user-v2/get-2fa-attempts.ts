import Request, { MetaResp } from "../request";

export interface TwofactorAttempts {
  numAttempts: number;
  maxAttempts: number;
  availableAt: number | null;
}

export default class Get2FaAttempts extends Request<MetaResp<TwofactorAttempts>, undefined> {
  handle(): Promise<MetaResp<TwofactorAttempts>> {
    return this.authReq({
      url: '/v2/user/auth/twofactor/attempts',
      method: 'GET'
    })
  }
}
