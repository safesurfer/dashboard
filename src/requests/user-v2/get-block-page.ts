import Request, { MetaResp } from '../request'

export interface APIBlockPage {
  customDomain: string;
  enabled: boolean;
  nxDomain: boolean;
}

export default class GetBlockPage extends Request<MetaResp<APIBlockPage>, undefined> {
  /**
   * @returns The user's custom block page settings.
   */
  handle(): Promise<MetaResp<APIBlockPage>> {
    return this.authReq({
      url: '/v2/user/block-page',
      method: 'GET'
    })
  }
}
