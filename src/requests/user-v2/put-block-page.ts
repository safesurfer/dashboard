import Request, { MetaResp } from '../request'
import { APIBlockPage } from './get-block-page'

export default class PutBlockPage extends Request<MetaResp<null>, APIBlockPage> {
  /**
   * Set the user's block page settings.
   * @param opts The settings.
   */
  handle(opts: APIBlockPage): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/user/block-page',
      method: 'PUT',
      data: opts
    })
  }
}
