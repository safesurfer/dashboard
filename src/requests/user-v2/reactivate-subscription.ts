import Request, { MetaResp } from "../request";

export default class ReactivateSubscription extends Request<MetaResp<null>, undefined> {
  handle(): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/user/subscription/reactivate',
      method: 'POST'
    })
  }

  /**
   * Given an axios error and promise reject function, reject
   * the promise. Default behavior to reject with error code.
   * @param err The axios error.
   * @param reject Reject function.
   */
   protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
