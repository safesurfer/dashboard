import Request, { MetaResp } from "../request"
import { APIAuthResponse } from "./post-auth-token";

export interface GetTokenFromOtpArgs {
  id: string;
  otp: string;
}

export default class GetTokenFromOtp extends Request<MetaResp<APIAuthResponse>, GetTokenFromOtpArgs> {
  handle(opts: GetTokenFromOtpArgs): Promise<MetaResp<APIAuthResponse>> {
    return this.simpleReq({
      url: '/v2/user/auth/token/from-otp',
      method: 'GET',
      params: opts
    })
  }

  /**
   * Given an axios error and promise reject function, reject
   * the promise. Default behavior to reject with error code.
   * @param err The axios error.
   * @param reject Reject function.
   */
  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
