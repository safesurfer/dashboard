import Request, { MetaResp } from "../request";
import { APIAuthResponse } from "./post-auth-token";

export interface GetTokenFromSigninTokenArgs {
  'signin-token': string;
  'device-id': string;
}

export default class GetTokenFromSigninToken extends Request<MetaResp<APIAuthResponse>, GetTokenFromSigninTokenArgs> {
  handle(opts: GetTokenFromSigninTokenArgs): Promise<MetaResp<APIAuthResponse>> {
    return this.simpleReq({
      url: '/v2/user/auth/token/from-signin-token',
      method: 'GET',
      params: opts
    })
  }
}