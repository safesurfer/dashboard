import { getCategorizerURL } from "@/helpers/functions";
import Request, { MetaResp } from "@/requests/request";

export interface DomainQueryResult {
  exactMatch: Domain
  superDomains: Domain[]
}

export interface Domain {
  id: number
  domain: string
  includeSubdomains: boolean
  useForTraining: boolean
  categoryIDs: number[]
  classifiedBy: string
  lastUpdated: number
}

export default class DomainsPublicSearch extends Request<MetaResp<DomainQueryResult>, string> {
  handle(opts: string): Promise<MetaResp<DomainQueryResult>> {
    return this.simpleReq({
      url: '/api/domains/public-search',
      params: {
        domain: opts
      },
      method: 'GET'
    })
  }

  getBaseURL () {
    return getCategorizerURL()
  }
}
