import { getCategorizerURL } from "@/helpers/functions";
import Request, { MetaResp } from "@/requests/request";

export interface Survey {
  id: number;
  name: string;
  maxSubsPerIp: number;
  steps: Step[];
}

export type StepType = 'text' | 'checkmulti' | 'radio'

export interface Step {
  type: StepType;
  title: string;
  nextStep?: number;
  textCharLimit?: number;
  options?: string[];
  optionNextSteps?: (number | null)[];
}

export default class GetSurvey extends Request<MetaResp<Survey>, number> {
  handle(opts: number): Promise<MetaResp<Survey>> {
    return this.authReq({
      url: '/api/surveys/' + opts
    })
  }

  getBaseURL () {
    return getCategorizerURL()
  }
}
