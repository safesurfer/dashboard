import Request, { MetaResp } from "../request";

export type Day = '*' | 'weekend' | 'weekday' | '0' | '1' | '2' | '3' | '4' | '5' | '6'

export interface TimerRunningStatus {
  lastStart: number;
  secsSoFar: number;
  isRunning: boolean;
}

export interface TimerSpec {
  deviceRef: DeviceRef;
  day: Day;
  maxTimeSecs: number;
  runningStatuses: {[day: number]: TimerRunningStatus};
  lastUpdated: number;
}

export interface TimetableSpec {
  deviceRef: DeviceRef;
  activeTimes: {[day: string]: boolean[]};
}

export interface DeadlineSpec {
  deviceRef: DeviceRef;
  expiry: number;
}

export interface ScreentimeRules {
  timezone: string;
  timers: {[id: number]: TimerSpec};
  timetables: {[id: number]: TimetableSpec};
  deadlines: {[id: number]: DeadlineSpec};
}

export interface ScreentimeRef<R> {
  id: number;
  ruleset: R;
}

export interface BlockingModel<R> {
  parents: BlockingModel<R>[];
  deviceRef: DeviceRef;
  baseRuleset: R;
  timers: ScreentimeRef<R>[];
  timetables: ScreentimeRef<R>[];
  deadlines: ScreentimeRef<R>[];
}

export interface BlockingModelResp<M, R, RR, PR> {
  meta: M;
  blockingModel: BlockingModel<R>;
  resolvedBlockingModel: RR;
  plan: PR;
  screentimeRules: ScreentimeRules;
}

export interface AppData {
  name: string;
  installedOn: string[];
  categories: number[];
  installedAt: number;
}

export interface AppsMetadata {
  appOrder: string[];
  appData: {[app: string]: AppData};
}

export interface AppModel {
  blocked: boolean;
  isFromCategory?: boolean;
}

export interface AppsRuleset {
  apps: {[app: string]: AppModel | null};
  approvalRequired: boolean | null;
}

export interface DeviceRef {
  accountId: string;
  deviceId: string;
  mac: string;
}

export interface BlockingSource {
  other: string;
  timerId: number | null;
  timetableId: number | null;
  deadlineId: number | null;
}

export interface BlockingReason {
  device: DeviceRef;
  source?: BlockingSource;
}

export interface ResolvedAppModel {
  blocked: boolean;
  isFromCategory: boolean;
  reason: BlockingReason;
}

export type ResolvedAppBlockingModel = {[app: string]: ResolvedAppModel}

export interface ActionPlan<A> {
  action: A;
  reason: BlockingReason;
  end: number;
}

export type AppBlockingPlan = {[app: string]: ActionPlan<AppModel>[]}

export type AppBlockingModelResp = BlockingModelResp<AppsMetadata, AppsRuleset, ResolvedAppBlockingModel, AppBlockingPlan>

export interface GetAppBlockingArgs {
  resolve?: boolean;
  'plan-ahead'?: string;
  'device-id'?: string;
}

export default class GetAppBlocking extends Request<MetaResp<AppBlockingModelResp>, GetAppBlockingArgs> {
  handle(opts: GetAppBlockingArgs): Promise<MetaResp<AppBlockingModelResp>> {
    return this.authReq({
      url: '/v2/blocking/apps',
      method: 'GET',
      params: opts
    })
  }
}
