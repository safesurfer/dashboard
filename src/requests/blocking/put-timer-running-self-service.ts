import Request, { MetaResp } from "../request";

export interface PutTimerRunningSelfServiceArgs {
  id: number;
  token: string;
  running: boolean;
  'device-id': string;
  'for-device'?: string;
  mac: string;
}

export default class PutTimerRunningSelfService extends Request<MetaResp<null>, PutTimerRunningSelfServiceArgs> {
  handle(opts: PutTimerRunningSelfServiceArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/blocking/self-service/screentime-rules/timer/' + opts.id + '/running',
      method: 'PUT',
      data: this.encodeAsForm({
        running: opts.running
      }),
      params: {
        'device-id': opts['device-id'],
        mac: opts.mac,
        'for-device': opts['for-device'],
      },
      auth: {
        username: 'self-service-token',
        password: opts.token
      }
    })
  }
}
