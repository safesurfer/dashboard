import Request, { MetaResp } from "../request";
import { TimerSpec } from "./get-app-blocking";

export default class PostTimer extends Request<MetaResp<number>, TimerSpec> {
  handle(opts: TimerSpec): Promise<MetaResp<number>> {
    return this.authReq({
      url: '/v2/blocking/screentime-rules/timers',
      method: 'POST',
      data: opts
    })
  }
}
