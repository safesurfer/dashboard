import Request, { MetaResp } from "../request";

export default class DeleteDeadline extends Request<MetaResp<null>, number> {
  handle(opts: number): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/blocking/screentime-rules/deadlines/' + opts,
      method: 'DELETE'
    })
  }
}
