import Request, { MetaResp } from "../request";
import { AppsMetadata } from "./get-app-blocking";

export default class GetAppBlockingMetadata extends Request<MetaResp<AppsMetadata>, string> {
  handle(opts: string): Promise<MetaResp<AppsMetadata>> {
    return this.authReq({
      url: '/v2/blocking/apps/meta',
      method: 'GET',
      params: {
        'device-id': opts
      }
    })
  }
}
