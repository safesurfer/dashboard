import { AppModel, BlockingReason } from "../blocking/get-app-blocking";
import { CategoryModel } from "../blocking/patch-category-blocking-base-ruleset";
import Request, { MetaResp } from "../request";

export interface ChangeResult {
  updated: boolean;
}

export interface ChangeResults {
  results: ChangeResult[] | null;
}

export interface Change<I, T> {
  id: I;
  model: T | null;
  ruleset: BlockingReason;
}

export interface DomainModel {
  blocked: boolean;
}

export interface BlockingChange {
  category?: Change<number, CategoryModel>;
  domain?: Change<string, DomainModel>;
  app?: Change<string, AppModel>;
}

export interface BlockingChanges {
  changes: BlockingChange[];
}

export interface PostBlockingChangesArgs {
  body: BlockingChanges;
  id: string;
  mac: string;
}

export default class PostBlockingChanges extends Request<MetaResp<ChangeResults>, PostBlockingChangesArgs> {
  handle(opts: PostBlockingChangesArgs): Promise<MetaResp<ChangeResults>> {
    return this.authReq({
      url: '/v2/blocking/changes',
      method: 'POST',
      params: {
        'device-id': opts.id,
        'mac': opts.mac
      },
      data: opts.body
    })
  }
}
