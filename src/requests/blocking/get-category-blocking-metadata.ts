import Request, { MetaResp } from "../request";
import { CategoryMetadata } from "./get-category-blocking";
import qs from 'qs'

export interface GetCategoryBlockingMetadataArgs {
  categoryList?: number[];
  includeNotBlockable: boolean;
}

export default class GetCategoryBlockingMetadata extends Request<MetaResp<CategoryMetadata>, GetCategoryBlockingMetadataArgs> {
  handle(opts: GetCategoryBlockingMetadataArgs): Promise<MetaResp<CategoryMetadata>> {
    const q: any = {
      'include-not-blockable': opts.includeNotBlockable
    }
    if (opts.categoryList) {
      q['category-list'] = opts.categoryList
    }
    return this.authReq({
      url: '/v2/blocking/categories/meta?' + qs.stringify(q, {
        arrayFormat: 'repeat'
      }),
      method: 'GET',
    })
  }
}
