import Request, { MetaResp } from "../request";

export interface PutTimerRunningArgs {
  id: number;
  running: boolean;
  'device-id': string;
  mac: string;
}

export default class PutTimerRunning extends Request<MetaResp<null>, PutTimerRunningArgs> {
  handle(opts: PutTimerRunningArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/blocking/screentime-rules/timers/' + opts.id + '/running',
      method: 'PUT',
      data: this.encodeAsForm({
        running: opts.running
      }),
      params: {
        'device-id': opts['device-id'],
        mac: opts.mac
      }
    })
  }
}
