import Request, { MetaResp } from "../request";
import { BlockingModelResp } from "./get-blocking";

export interface GetBlockArgsSelfService {
  'device-id'?: string;
  'mac'?: string;
  'plan-ahead'?: string;
  'resolve'?: boolean;
  token: string;
}

export default class GetBlocking extends Request<MetaResp<BlockingModelResp>, GetBlockArgsSelfService> {
  handle(opts: GetBlockArgsSelfService): Promise<MetaResp<BlockingModelResp>> {
    return this.simpleReq({
      url: '/v2/blocking/self-service',
      method: 'GET',
      params: {
        'device-id': opts['device-id'],
        mac: opts.mac,
        'plan-ahead': opts['plan-ahead'],
        resolve: opts.resolve
      },
      auth: {
        username: 'self-service-token',
        password: opts.token
      }
    })
  }
}
