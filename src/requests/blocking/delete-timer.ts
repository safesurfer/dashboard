import Request, { MetaResp } from "../request";

export default class DeleteTimer extends Request<MetaResp<null>, number> {
  handle(opts: number): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/blocking/screentime-rules/timers/' + opts,
      method: 'DELETE'
    })
  }
}
