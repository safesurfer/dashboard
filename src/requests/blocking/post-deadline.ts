import Request, { MetaResp } from "../request";
import { DeadlineSpec } from "./get-app-blocking";

export default class PostDeadline extends Request<MetaResp<number>, DeadlineSpec> {
  handle(opts: DeadlineSpec): Promise<MetaResp<number>> {
    return this.authReq({
      url: '/v2/blocking/screentime-rules/deadlines',
      method: 'POST',
      data: opts
    })
  }
}
