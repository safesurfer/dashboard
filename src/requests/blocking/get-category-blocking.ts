import Request, { MetaResp } from "../request";
import { ActionPlan, BlockingModelResp, BlockingReason } from "./get-app-blocking";
import { CategoriesRuleset, CategoryModel } from "./patch-category-blocking-base-ruleset";

export interface CategoryOptionData {
  name: string;
}

export interface CategoryOptionsMetadata {
  options: CategoryOptionData[];
  offOption?: number;
  blockingOption?: number;
}

export interface CategoryData {
  name: string;
  icon: string;
  frozen: boolean;
  description: string;
  parent: number;
  displayed: boolean;
  alertDefault: boolean;
  collapse: boolean;
  options?: CategoryOptionsMetadata;
}

export interface CategoryMetadata {
  categoryOrder: number[];
  categoryData: {[id: number]: CategoryData};
}

export interface ResolvedCategoryModel {
  blocked?: boolean;
  selectedOption?: number;
  reason: BlockingReason;
}

export type ResolvedCategoryBlockingModel = {[id: number]: ResolvedCategoryModel}

export type CategoryBlockingPlan = {[id: number]: ActionPlan<CategoryModel>[]}

export type CategoryBlockingModelResp = BlockingModelResp<CategoryMetadata, CategoriesRuleset, ResolvedCategoryBlockingModel, CategoryBlockingPlan>

export interface GetCategoryBlockingArgs {
  resolve?: boolean;
  'plan-ahead'?: string;
  'device-id'?: string;
  mac?: string;
  'category-list'?: number[];
  'exclude-screentime-rules'?: boolean;
  'exclude-parents'?: boolean;
}

export default class GetCategoryBlocking extends Request<MetaResp<CategoryBlockingModelResp>, GetCategoryBlockingArgs> {
  handle(opts: GetCategoryBlockingArgs): Promise<MetaResp<CategoryBlockingModelResp>> {
    return this.authReq({
      url: '/v2/blocking/categories',
      method: 'GET',
      params: opts
    })
  }
}
