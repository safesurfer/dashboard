import Request, { MetaResp } from "../request";
import { TimetableSpec } from "./get-app-blocking";

export default class PostTimetable extends Request<MetaResp<number>, TimetableSpec> {
  handle(opts: TimetableSpec): Promise<MetaResp<number>> {
    return this.authReq({
      url: '/v2/blocking/screentime-rules/timetables',
      method: 'POST',
      data: opts
    })
  }
}
