import Request, { MetaResp } from "../request";
import { TimerSpec } from "./get-app-blocking";

export interface PutTimerArgs {
  id: number;
  spec: TimerSpec;
}

export default class PutTimer extends Request<MetaResp<null>, PutTimerArgs> {
  handle(opts: PutTimerArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/blocking/screentime-rules/timers/' + opts.id,
      method: 'PUT',
      data: opts.spec
    })
  }
}
