import Request, { MetaResp } from "../request";
import { AppsRuleset } from "./get-app-blocking";

export interface PatchAppBlockingBaseRulesetArgs {
  deviceId: string;
  ruleset: AppsRuleset;
}

export default class PatchAppBlockingBaseRuleset extends Request<MetaResp<null>, PatchAppBlockingBaseRulesetArgs> {
  handle(opts: PatchAppBlockingBaseRulesetArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/blocking/apps/model/base-ruleset',
      method: 'PATCH',
      data: opts.ruleset,
      params: {
        'device-id': opts.deviceId
      }
    })
  }
}
