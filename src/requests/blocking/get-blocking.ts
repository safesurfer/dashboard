import Request, { MetaResp } from "../request";
import { ActionPlan, AppBlockingModelResp, BlockingReason, ScreentimeRules } from "./get-app-blocking";
import { CategoryBlockingModelResp } from "./get-category-blocking";
import { BlockingModelResp as BaseBlockingModelResp } from './get-app-blocking'
import { DomainModel } from "./post-blocking-changes";

export interface DomainData {}

export interface DomainMetadata {
  domainOrder: string[];
  domainData: {[domain: string]: DomainData}
}

export interface DomainsRuleset {
  domains: {[domain: string]: DomainModel}
}

export interface ResolvedDomainModel {
  blocked: boolean;
  reason: BlockingReason;
}

export type ResolvedDomainBlockingModel = {[domain: string]: ResolvedDomainModel}

export type DomainBlockingPlan = {[domain: string]: ActionPlan<DomainModel>[]}

export type DomainBlockingModelResp = BaseBlockingModelResp<DomainMetadata, DomainsRuleset, ResolvedDomainBlockingModel, DomainBlockingPlan>

export interface BlockingModelResp {
  categories: CategoryBlockingModelResp;
  apps: AppBlockingModelResp;
  domains: DomainBlockingModelResp;
  screentimeRules: ScreentimeRules;
}

export interface GetBlockArgs {
  'device-id'?: string;
  'mac'?: string;
  'plan-ahead'?: string;
  'resolve'?: boolean;
  'include-not-installed-apps'?: boolean;
}

export default class GetBlocking extends Request<MetaResp<BlockingModelResp>, GetBlockArgs> {
  handle(opts: GetBlockArgs): Promise<MetaResp<BlockingModelResp>> {
    return this.authReq({
      url: '/v2/blocking',
      method: 'GET',
      params: opts
    })
  }
}
