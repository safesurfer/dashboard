import Request, { MetaResp } from "../request";
import { TimetableSpec } from "./get-app-blocking";

export interface PutTimetableArgs {
  id: number;
  spec: TimetableSpec;
}

export default class PutTimetable extends Request<MetaResp<null>, PutTimetableArgs> {
  handle(opts: PutTimetableArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/blocking/screentime-rules/timetables/' + opts.id,
      method: 'PUT',
      data: opts.spec
    })
  }
}
