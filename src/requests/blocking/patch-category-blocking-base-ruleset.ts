import Request, { MetaResp } from "../request";

export interface CategoryModel {
  blocked?: boolean;
  selectedOption?: number;
}

export interface CategoriesRuleset {
  categories: {[id: number]: CategoryModel | null}
}

export interface PatchCategoryBlockingBaseRulesetArgs {
  'device-id': string;
  'mac': string;
  ruleset: CategoriesRuleset;
}

export default class PatchCategoryBlockingBaseRuleset extends Request<MetaResp<null>, PatchCategoryBlockingBaseRulesetArgs> {
  handle(opts: PatchCategoryBlockingBaseRulesetArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/blocking/categories/model/base-ruleset',
      method: 'PATCH',
      params: {
        'device-id': opts["device-id"],
        'mac': opts['mac']
      },
      data: opts.ruleset
    })
  }
}
