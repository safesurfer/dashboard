import Request from '@/requests/request'

export interface PutDeviceNameOptions {
  /**
   * The device ID to change the name for.
   */
  id: string;
  /**
   * The MAC address of the device to change the name for, if any.
   */
  mac?: string;
  /**
   * The new name.
   */
  name: string;
}

export default class PutDeviceName extends Request<undefined, PutDeviceNameOptions> {
  /**
   * Change the name of a device.
   * @param opts The ID and new name of the device.
   */
  handle(opts: PutDeviceNameOptions): Promise<undefined> {
    return this.authReq({
      url: '/devices/device/' + encodeURIComponent(opts.id) + '/name',
      method: 'PUT',
      params: {
        'mac': opts.mac
      },
      data: this.encodeAsForm({
        name: opts.name
      }),
    })
  }
}
