import Request from "../request"

export default class PutDeviceIP extends Request<null, string> {
  handle(opts: string): Promise<null> {
    return this.authReq({
      url: '/devices/device/' + opts + '/ip',
      method: 'PUT' 
    })
  }
}
