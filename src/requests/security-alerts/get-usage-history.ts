import Request, { MetaResp } from "../request";

export interface UsageHistoryItem {
  timestamp: number;
  wasLimited: boolean;
  quantity: number;
  maxQuantity: number;
}

export default class GetUsageHistory extends Request<MetaResp<UsageHistoryItem[]>, undefined> {
  handle(): Promise<MetaResp<UsageHistoryItem[]>> {
    return this.authReq({
      url: '/v2/security-alerts/usage',
      method: 'GET'
    })
  }
}
