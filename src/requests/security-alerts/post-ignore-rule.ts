import Request, { MetaResp } from "../request";
import { IgnoreRule } from "./get-ignore-rules";

export default class PostIgnoreRule extends Request<MetaResp<number>, IgnoreRule> {
  handle(opts: IgnoreRule): Promise<MetaResp<number>> {
    return this.authReq({
      url: '/v2/security-alerts/ignore-rules',
      method: 'POST',
      data: opts
    })
  }
}
