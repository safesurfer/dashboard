import Request, { MetaResp } from "../request";

export interface IgnoreRule {
  id: number;
  deviceID: string;
  mac: string;
  domain: string;
  threat: string;
  threatTitle: string;
  detectionKey: string;
}

export default class GetIgnoreRules extends Request<MetaResp<IgnoreRule[]>, undefined> {
  handle(): Promise<MetaResp<IgnoreRule[]>> {
    return this.authReq({
      url: '/v2/security-alerts/ignore-rules',
      method: 'GET'
    })
  }
}
