import Request, { MetaResp } from "../request";

export interface Threat {
  id: string;
  title: string;
  description: string;
  severity: number;
}

export default class GetThreats extends Request<MetaResp<Threat[]>, undefined> {
  handle(): Promise<MetaResp<Threat[]>> {
    return this.simpleReq({
      url: '/v2/security-alerts/threats',
      method: 'GET'
    })
  }
}
