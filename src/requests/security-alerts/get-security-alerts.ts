import Request, { MetaResp } from "../request";

export interface GetSecurityAlertsArgs {
  'device-id': string;
  mac: string;
  'num-per-page': number;
  page: number;
  threats: string[];
  'start-timestamp'?: number;
  'start-date'?: string;
  'end-timestamp'?: number;
  'end-date'?: string;
  'tz'?: string;
}

export interface SecurityAlert {
  idx: number; // internal
  deviceID: string;
  mac: string;
  timestamp: number;
  domain: string;
  threat: string;
  threatTitle: string;
  threatDescription: string;
  detectionKey: string;
  severity: number;
}

export interface SecurityAlerts {
  more: boolean;
  alerts: SecurityAlert[];
}

export default class GetSecurityAlerts extends Request<MetaResp<SecurityAlerts>, GetSecurityAlertsArgs> {
  handle(opts: GetSecurityAlertsArgs): Promise<MetaResp<SecurityAlerts>> {
    return this.authReq({
      url: '/v2/security-alerts',
      params: opts
    })
  }
}
