import Request from '@/requests/request'

export interface VPNAttempt {
  /**
   * The ID of the attempt.
   */
  id: number;
  /**
   * The MAC of the device that made the attempt.
   */
  mac: string;
  /**
   * The protocol used for the attempt.
   */
  protocol: string;
  /**
   * The source IP.
   */
  sourceIP: string;
  /**
   * The destination IP.
   */
  destIP: string;
  /**
   * The source port.
   */
  sourcePort: number;
  /**
   * The destination port.
   */
  destPort: number;
  /**
   * Amount of MB sent.
   */
  connectionSize: number;
  /**
   * Time the connection started.
   * Unix timestamp, seconds.
   */
  started: number;
}

export default class GetVPNAttempts extends Request<VPNAttempt[], string> {
  /**
   * @returns The VPN attempts for a lifeguard device.
   * @param opts The lifeguard device ID.
   */
  handle(opts: string): Promise<VPNAttempt[]> {
    return this.authReq({
      url: '/vpns/attempts?deviceID=' + encodeURIComponent(opts),
      method: 'GET'
    })
  }
}