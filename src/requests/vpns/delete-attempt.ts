import Request from '@/requests/request'

export interface DeleteVPNAttemptOpts {
  /**
   * The ID of the attempt to delete.
   */
  id: number;
  /**
   * Whether to flag is innaccurate.
   */
  innaccurate: boolean;
}

export default class DeleteVPNAttempt extends Request<undefined, DeleteVPNAttemptOpts> {
  /**
   * Delete a VPN attempt by ID.
   * @param opts The id and whether innaccurate.
   */
  handle (opts: DeleteVPNAttemptOpts): Promise<undefined> {
    return this.authReq({
      url: '/vpns/attempts/' + opts.id,
      method: 'DELETE',
      params: {
        innaccurate: opts.innaccurate
      }
    })
  }
}
