import Request, { MetaResp } from "../request";
import { Quota } from "./get-quota";

export default class GetQuotas extends Request<MetaResp<{[quotaName: string]: Quota}>, undefined> {
  handle (): Promise<MetaResp<{ [quotaName: string]: Quota; }>> {
    return this.authReq({
      url: '/v2/quotas',
      method: 'GET'
    })
  }
}
