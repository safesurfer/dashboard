import Request, { MetaResp } from "../request";
import { GetCategoriesParams } from "./get-blocked-categories";

export interface Activity {
  blocks: number[];
  views: number[];
}

export default class GetActivity extends Request<MetaResp<Activity>, GetCategoriesParams> {
  handle(opts: GetCategoriesParams): Promise<MetaResp<Activity>> {
    return this.authReq({
      url: '/v2/usage/activity',
      method: 'GET',
      params: opts
    })
  }
}