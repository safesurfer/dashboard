import Request, { MetaResp } from "../request"

export interface DeleteAllUsageHistoryArgs {
  'device-id': string
  mac: string
}

export default class DeleteAllUsageHistory extends Request<MetaResp<null>, DeleteAllUsageHistoryArgs> {
  /**
   * Delete all usage data for the account or device.
   * @param opts The device ID to delete data for, or blank to delete data for the whole account.
   */
  handle(opts: DeleteAllUsageHistoryArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/usage/history/delete-all',
      method: 'POST',
      data: this.encodeAsForm(opts)
    })
  }
}