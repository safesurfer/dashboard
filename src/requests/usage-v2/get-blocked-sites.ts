import Request, { MetaResp } from "../request"
import { GetCategoriesParams } from "./get-blocked-categories"

export interface BlockedSites {
  blocks: {[catID: number]: BlockedSitesForCategory};
  categoryNames: {[catID: number]: string};
  categoryDescriptions: {[catID: number]: string};
  categoryIcons: {[catID: number]: string};
}

export interface BlockedSitesForCategory {
  total: number;
  sites: BlockedSite[];
}

export interface BlockedSite {
  domain: string;
  count: number;
}

export default class GetBlockedSites extends Request<MetaResp<BlockedSites>, GetCategoriesParams> {
  handle(opts: GetCategoriesParams): Promise<MetaResp<BlockedSites>> {
    return this.authReq({
      url: '/v2/usage/blocked-sites',
      method: 'GET',
      params: opts
    })
  }
}
