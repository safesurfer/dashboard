import Request, { MetaResp } from "../request";
import { UsageHistoryItem } from "./get-history";

export default class DeleteUsageHistory extends Request<MetaResp<null>, UsageHistoryItem[]> {
  handle(opts: UsageHistoryItem[]): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/usage/history/delete',
      method: 'POST',
      data: opts
    })
  }
}
