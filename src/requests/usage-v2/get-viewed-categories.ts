import Request, { MetaResp } from "../request";
import { CategoryCount, GetCategoriesParams } from "./get-blocked-categories";

export default class GetViewedCategories extends Request<MetaResp<CategoryCount[]>, GetCategoriesParams> {
  handle(opts: GetCategoriesParams): Promise<MetaResp<CategoryCount[]>> {
    return this.authReq({
      url: '/v2/usage/viewed-categories',
      method: 'GET',
      params: opts
    })
  }
}
