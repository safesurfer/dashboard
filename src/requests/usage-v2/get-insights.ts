import Request, { MetaResp } from "../request";
import { GetCategoriesParams } from "./get-blocked-categories";

export interface GetInsightsParams extends GetCategoriesParams {
  max: number;
}

export interface Insight {
  name: string;
  percent: number;
  quantity: number;
  categoryID: string;
  children: Insight[];
  icon: string;
}

export interface Insights {
  insights: Insight[];
  trimmed: boolean;
}

export default class GetInsights extends Request<MetaResp<Insights>, GetInsightsParams> {
  handle(opts: GetInsightsParams): Promise<MetaResp<Insights>> {
    return this.authReq({
      url: '/v2/usage/insights',
      method: 'GET',
      params: opts
    })
  }
}
