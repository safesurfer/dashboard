import Request from '@/requests/request'

export interface PostPasswordOptions {
  /**
   * User's email.
   */
  email: string;
  /**
   * User's old password.
   */
  oldPass: string;
  /**
   * The new password.
   */
  password: string;
}

export default class PostPassword extends Request<string, PostPasswordOptions> {
  /**
   * Set the user's new password.
   * @param opts The form values.
   */
  handle (opts: PostPasswordOptions): Promise<string> {
    return this.authReq({
      url: '/user/password',
      method: 'POST',
      data: this.encodeAsForm(opts)
    })
  }

  protected redirectOn401 (): boolean {
    return false
  }
}
