import Request from '@/requests/request'

export default class PostSignOutAll extends Request<undefined, undefined> {
  /**
   * Sign out of all devices on the user's account.
   */
  handle (): Promise<undefined> {
    return this.authReq({
      url: '/user/sign-out-all',
      method: 'POST'
    })
  }
}
