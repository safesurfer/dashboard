import Request from '@/requests/request'

export default class GetVerification extends Request<boolean, undefined> {
  /**
   * @returns Whether the user's account is verified.
   */
  handle (): Promise<boolean> {
    return this.authReq({
      url: '/user/verification',
      method: 'GET'
    })
  }
}
