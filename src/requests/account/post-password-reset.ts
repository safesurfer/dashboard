import Request from '@/requests/request'

export interface PostPasswordResetOptions {
  // The JWT token used as a key to identify the user.
  key: string;
  // The new password to set
  password: string;
}

export default class PostPasswordReset extends Request<boolean, PostPasswordResetOptions> {
  /**
   * @returns Whether the key used was too old.
   * @param opts The form options.
   */
  handle (opts: PostPasswordResetOptions): Promise<boolean> {
    return this.simpleReq({
      url: '/user/password/reset',
      method: 'POST',
      data: this.encodeAsForm(opts)
    })
  }

  /**
   * Given an axios error and promise reject function, reject
   * the promise. Default behavior to reject with error code.
   * @param err The axios error.
   * @param reject Reject function.
   */
  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
