import Request from '@/requests/request'

export default class GetPublicKey extends Request<string, undefined> {
  /**
   * @returns The VAPID public key of the notification service.
   */
  handle(): Promise<string> {
    return this.simpleReq({
      url: '/notifications/web-push/key',
      method: 'GET'
    })
  }
}
