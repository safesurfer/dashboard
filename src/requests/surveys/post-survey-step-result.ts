import { AxiosRequestConfig } from "axios";
import Request, { MetaResp } from "../request";

export interface PostSurveyStepResultArgs {
  surveyID: number;
  stepNum: number;
  result: SurveyStepResult;
  authenticated: boolean;
}

export interface SurveyStepResult {
  textResult?: string;
  selectedOption?: number;
  selectedOptions?: number[];
}

export default class PostSurveyStepResult extends Request<MetaResp<null>, PostSurveyStepResultArgs> {
  handle(opts: PostSurveyStepResultArgs): Promise<MetaResp<null>> {
    const args: AxiosRequestConfig = {
      url: '/v2/surveys/' + opts.surveyID + '/step/' + opts.stepNum + '/result',
      method: 'POST',
      data: opts.result
    }
    if (opts.authenticated) {
      return this.authReq(args)
    } else {
      return this.simpleReq(args)
    }
  }
}
