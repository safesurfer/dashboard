import Request, { MetaResp } from "../request";

export interface AlertsResult {
  alerts: Alert[];
  more: boolean;
}

export type AlertOrigin = 'REQUEST' | 'SCREENSHOT' | 'DEVICE_STATUS' | 'EVASION_ATTEMPT' | 'VPN_ATTEMPT' | 'LOGGED_NOTIFICATION' | 'NOACTIVITY' | 'SECURITY_ALERTS' | 'INSTALLED_APP'

export interface Alert {
  id: string;
  origin: AlertOrigin;
  timestamp: number;
  timestampEnd: number;
  deviceId: string;
  deviceMac: string;
  display: AlertDisplaySection[];
  data: {[key: number]: string};
  priority: number;
  summarized: Alert[];
}

export type AlertDisplaySectionType = 'DEVICE_LINK' | 'TEXT' | 'LIGHT_TEXT' | 'BOLD_TEXT' | 'MORE_INFO_LINK'

export interface AlertDisplaySection {
  type: AlertDisplaySectionType;
  text: string;
}

export interface GetAlertsArgs {
  'num-per-page': number;
  page: number;
  start: number;
}

export default class GetAlerts extends Request<MetaResp<AlertsResult>, GetAlertsArgs> {
  handle(opts: GetAlertsArgs): Promise<MetaResp<AlertsResult>> {
    return this.authReq({
      url: '/v2/alerts',
      params: opts,
      method: 'GET'
    })
  }
}
