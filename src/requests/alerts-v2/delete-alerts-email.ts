import Request, { MetaResp } from "../request";

export default class DeleteAlertsEmail extends Request<MetaResp<null>, number> {
  handle(opts: number): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/alerts/emails/' + opts,
      method: 'DELETE'
    })
  }
}
