import Request, { MetaResp } from "../request";
import { EmailSettings } from "./get-alerts-emails";

export default class PutAlertEmail extends Request<MetaResp<null>, EmailSettings> {
  handle(opts: EmailSettings): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/alerts/emails/' + opts.id,
      method: 'PUT',
      data: opts
    })
  }
}
