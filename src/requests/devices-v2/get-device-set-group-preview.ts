import { BlockingModelResp } from "../blocking/get-blocking";
import Request, { MetaResp } from "../request";

export interface BlockingModelDeviceDiff {
  before: BlockingModelResp;
  after: BlockingModelResp;
  categoryChanges: number[];
  appChanges: string[];
  domainChanges: string[];
}

export interface GetDeviceSetGroupPreviewArgs {
  'plan-ahead'?: string;
  id: string;
  mac?: string;
  'new-group': string;
}

export default class GetDeviceSetGroupPreview extends Request<MetaResp<BlockingModelDeviceDiff>, GetDeviceSetGroupPreviewArgs> {
  handle(opts: GetDeviceSetGroupPreviewArgs): Promise<MetaResp<BlockingModelDeviceDiff>> {
    return this.authReq({
      url: '/v2/devices/' + encodeURIComponent(opts.id) + '/set-group-preview',
      method: 'GET',
      params: {
        'plan-ahead': opts['plan-ahead'],
        mac: opts.mac,
        'new-group': opts['new-group']
      }
    })
  }
}
