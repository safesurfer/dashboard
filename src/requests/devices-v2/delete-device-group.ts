import Request, { MetaResp } from "../request";

export interface DeleteDeviceGroupArgs {
  id: string;
  mac: string;
}

export default class DeleteDeviceGroup extends Request<MetaResp<null>, DeleteDeviceGroupArgs> {
  handle(opts: DeleteDeviceGroupArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/devices/' + encodeURIComponent(opts.id) + '/group',
      method: 'DELETE',
      params: {
        mac: opts.mac
      }
    })
  }
}
