import Request, { MetaResp } from "../request";

export default class GetMetadata extends Request<MetaResp<{[key: string]: string}>, undefined> {
  handle(): Promise<MetaResp<{ [key: string]: string; }>> {
    return this.authReq({
      url: '/v2/devices/metadata',
      method: 'GET'
    })    
  }
}
