import Request, { MetaResp } from "../request";

export interface PostIosRemovalPasswordArgs {
  udid: string;
  deviceID: string;
}

export default class PostIosRemovalPassword extends Request<MetaResp<null>, PostIosRemovalPasswordArgs> {
  handle(opts: PostIosRemovalPasswordArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/devices/' + opts.deviceID + '/ios-removal-password',
      method: 'POST',
      data: JSON.stringify(opts.udid),
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }

  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
