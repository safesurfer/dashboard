import Request, { MetaResp } from "../request";
import { PostSelfServiceTokenArgs } from "./post-self-service-token";

export default class GetSelfServiceToken extends Request<MetaResp<string>, PostSelfServiceTokenArgs> {
  handle(opts: PostSelfServiceTokenArgs): Promise<MetaResp<string>> {
    return this.authReq({
      url: '/v2/devices/self-service-token',
      method: 'GET',
      params: opts
    })
  }
}
