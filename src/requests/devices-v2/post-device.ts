import Request, { MetaResp } from "../request";
import { DeviceType } from "./get-devices-list";

export interface DeviceCreationArgs {
  type: DeviceType;
  name: string;
  icon: string;
  metadata: {[key: string]: string};
}

export interface DeviceCreationDetails {
  id: string;
  dnsToken: string;
}

export default class PostDevice extends Request<MetaResp<DeviceCreationDetails>, DeviceCreationArgs> {
  handle(opts: DeviceCreationArgs): Promise<MetaResp<DeviceCreationDetails>> {
    return this.authReq({
      url: '/v2/devices',
      method: 'POST',
      data: opts
    })
  }
}
