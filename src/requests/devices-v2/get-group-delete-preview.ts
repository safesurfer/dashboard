import Request, { MetaResp } from "../request";
import { BlockingModelDeviceDiff } from "./get-device-set-group-preview";

export interface BlockingModelDiff {
  devices: {[device: string]: BlockingModelDeviceDiff}
}

export interface GetGroupDeletePreviewArgs {
  id: string;
  'plan-ahead': string;
}

export default class GetGroupDeletePreview extends Request<MetaResp<BlockingModelDiff>, GetGroupDeletePreviewArgs> {
  handle(opts: GetGroupDeletePreviewArgs): Promise<MetaResp<BlockingModelDiff>> {
    return this.authReq({
      url: '/v2/devices/' + encodeURIComponent(opts.id) + '/group-delete-preview',
      method: 'GET',
      params: {
        'plan-ahead': opts['plan-ahead']
      }
    })
  }
}
