import Request, { MetaResp } from "../request";

export type DeviceType = 'group' | 'router' | 'router-client' | 'dns' | 'android' | 'ios' | 'windows'

export interface Device {
  type: DeviceType;
  name: string;
  icon: string;
  metadata: {[key: string]: string} | null;
  id: string;
  mac: string;
  groupName: string;
  version: string;
  nextVersion: string;
  registeredTime: number;
  lastActivity: number;
  parents: string[] | null;
  children: string[] | null;
}

export type ActivityState = 'OK' | 'OFF_REQUESTED' | 'OFF_LOGGING' | 'OFF_CONFIG' | 'OFF_ERROR'

export interface DevicesList {
  activityStatus: ActivityState;
  devices: {[id: string]: Device} | null;
  deviceOrder: string[] | null;
}

export type DeviceListSort = 'active-first' | 'inactive-first' | 'name-a-z' | 'name-z-a' | 'id-a-z' | 'id-z-a' | 'mac-a-z' | 'mac-z-a' | 'type-a-z' | 'type-z-a' | 'registered-new-first' | 'registered-old-first' | 'group-name-a-z' | 'group-name-z-a'

export interface GetDevicesListArgs {
  sort?: DeviceListSort;
  'get-activity'?: boolean;
  'device-id'?: string;
  mac?: string;
}

export default class GetDevicesList extends Request<MetaResp<DevicesList>, GetDevicesListArgs> {
  handle(opts: GetDevicesListArgs): Promise<MetaResp<DevicesList>> {
    return this.authReq({
      url: '/v2/devices/list',
      method: 'GET',
      params: opts
    })
  }
}
