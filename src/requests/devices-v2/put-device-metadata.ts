import Request, { MetaResp } from "../request";

export interface PutDeviceMetadataArgs {
  id: string;
  key: string;
  val: string;
}

export default class PutDeviceMetadata extends Request<MetaResp<null>, PutDeviceMetadataArgs> {
  handle(opts: PutDeviceMetadataArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/devices/' + opts.id + '/metadata/' + opts.key,
      method: 'PUT',
      data: JSON.stringify(opts.val),
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }
}
