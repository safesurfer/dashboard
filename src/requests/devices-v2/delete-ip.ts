import Request, { MetaResp } from "../request"

export default class DeleteIp extends Request<MetaResp<null>, string> {
  handle(opts: string): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/devices/' + opts + '/ip',
      method: 'DELETE'
    })
  }
}
