import Request, { MetaResp } from "../request";

export default class LogoutDevice extends Request<MetaResp<null>, string> {
  handle(opts: string): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/devices/' + encodeURIComponent(opts) + '/signout',
      method: 'POST'
    })
  }
}
