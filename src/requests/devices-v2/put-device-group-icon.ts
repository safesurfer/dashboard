import Request, { MetaResp } from "../request";

export interface PutDeviceGroupIconArgs {
  id: string;
  icon: string;
}

export default class PutDeviceGroupIcon extends Request<MetaResp<null>, PutDeviceGroupIconArgs> {
  handle(opts: PutDeviceGroupIconArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/devices/' + encodeURIComponent(opts.id) + '/icon',
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      data: opts.icon
    })
  }
}
