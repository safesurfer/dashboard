import Request, { MetaResp } from "../request";

export interface PutDeviceGroupArgs {
  id: string;
  mac: string;
  newGroup: string;
}

export default class PutDeviceGroup extends Request<MetaResp<null>, PutDeviceGroupArgs> {
  handle(opts: PutDeviceGroupArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/devices/' + encodeURIComponent(opts.id) + '/group',
      method: 'PUT',
      params: {
        mac: opts.mac,
      },
      headers: {
        'Content-Type': 'application/json'
      },
      data: opts.newGroup
    })
  }
}
