import Request, { MetaResp } from "../request";
import { PostSelfServiceTokenArgs } from "./post-self-service-token";

export default class DeleteSelfServiceToken extends Request<MetaResp<null>, PostSelfServiceTokenArgs> {
  handle(opts: PostSelfServiceTokenArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/devices/self-service-token',
      method: 'DELETE',
      params: opts
    })
  }
}
