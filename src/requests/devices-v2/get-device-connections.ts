import Request, { MetaResp } from "../request";

export interface ConnectionInfo {
  ip: string;
  ip6: string;
  dnsToken: string;
}

export default class GetDeviceConnections extends Request<MetaResp<ConnectionInfo>, string> {
  handle(opts: string): Promise<MetaResp<ConnectionInfo>> {
    return this.authReq({
      url: '/v2/devices/' + encodeURIComponent(opts) + '/connections',
      method: 'GET'
    })
  }
}
