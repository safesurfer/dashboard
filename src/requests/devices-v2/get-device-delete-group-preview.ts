import Request, { MetaResp } from "../request";
import { BlockingModelDeviceDiff } from "./get-device-set-group-preview";

export interface GetDeviceDeleteGroupPreviewArgs {
  'plan-ahead'?: string;
  id: string;
  mac?: string;
}

export default class GetDeviceDeleteGroupPreview extends Request<MetaResp<BlockingModelDeviceDiff>, GetDeviceDeleteGroupPreviewArgs> {
  handle(opts: GetDeviceDeleteGroupPreviewArgs): Promise<MetaResp<BlockingModelDeviceDiff>> {
    return this.authReq({
      url: '/v2/devices/' + encodeURIComponent(opts.id) + '/delete-group-preview',
      method: 'GET',
      params: {
        'plan-ahead': opts['plan-ahead'],
        mac: opts.mac,
      }
    })
  }
}
