import Request, { MetaResp } from "../request";

export interface PostSelfServiceTokenArgs {
  'device-id': string;
  mac: string;
}

export default class PostSelfServiceToken extends Request<MetaResp<null>, PostSelfServiceTokenArgs> {
  handle(opts: PostSelfServiceTokenArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/devices/self-service-token',
      method: 'POST',
      params: opts
    })
  }
}
