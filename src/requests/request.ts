import axios, { AxiosRequestConfig } from 'axios'
import { TOKEN_STORAGE_NAME } from '@/helpers/constants'
import { getBaseURL, log } from '@/helpers/functions'
import router from '@/router'
import qs from 'qs'

/**
 * Response metadata as returned by API v2.
 */
export interface MetaRespMetadata {
  timestamp: number;
  errorCode: string;
  code: number;
  message: string;
}

/**
 * Metadata response as returned by API v2.
 */
export interface MetaResp<T> {
  /**
   * Response metadata.
   */
  meta: MetaRespMetadata;
  /**
   * The return data.
   */
  spec: T;
}

/**
 * Metadata list response as returned by API v2.
 */
export interface MetaListResp<T> {
  meta: MetaRespMetadata;
  /**
   * How many items are available total. If paginated, this may not = the length of below.
   */
  available: number;
  /**
   * The items.
   */
  list?: T[];
}

// Global custom axios config
const customAxios = axios.create({
  paramsSerializer: {
    serialize: params => qs.stringify(params, { arrayFormat: 'repeat' })
  }
})

/**
 * Base class for creating a request.
 * Makes it easy to test components, e.g. checking
 * that a component made a request or giving it fake data.
 */
export default abstract class Request<T, D> {

  /**
   * Normally we get an auth token from local storage. But if this is defined, use it instead.
   */
  private auth: string | undefined;

  /**
   * Override the normal auth that we would use.
   * @param auth The auth token to use instead.
   */
  setAuth (auth: string) {
    this.auth = auth
  }

  /**
   * Redirect to the login page.
   * @param reason Display to the user why they are redirected.
   */
  protected loginRedirect (reason: string) {
    if (router.currentRoute.name === 'Login') {
      return
    }
    router.push({
      name: 'Login',
      query: {
        reason: reason,
        redirect: router.currentRoute.fullPath
      }
    })
  }

  /**
   * Given an axios error and promise reject function, reject
   * the promise. Default behavior to reject with error code.
   * @param err The axios error.
   * @param reject Reject function.
   */
  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.status) {
      reject(err.response.status)
    } else {
      reject() // eslint-disable-line
    }
  }

  /**
   * Make a request to the server.
   * @returns A promise resolving with the data of the reuqest
   *          when the request is completed, or rejecting with
   *          the http code if it fails.
   * @param config The axios config. URL should be relative.
   * @param inSpec  Whether the reply is an object, in which case it will be in the 'spec' of the request.
   *                Otherwise, it will be in metadata.response
   */
  protected simpleReq (config: AxiosRequestConfig): Promise<T> {
    return new Promise((resolve, reject) => {
      config.url = this.getBaseURL() + config.url
      log('config')
      log(config)
      customAxios(config).then(res => {
        log('request completed')
        log(res)
        // Everything ok
        resolve(res.data)
      }).catch(err => {
        log('request error')
        log(err.response)
        // Error of some kind
        log(config.url)
        this.rejector(err, reject)
      })
    })
  }

  /**
   * Make an authenticated request, or redirect to the
   * login page if we are not authenticated.
   * @returns A promise resolving with the data of the request
   *          when the request is completed, or rejecting with
   *          the http code if it fails.
   * @param config The axios config. URL should be relative.
   * @param inSpec  Whether the reply is an object, in which case it will be in the 'spec' of the request.
   *                Otherwise, it will be in metadata.response
   */
  protected authReq (config: AxiosRequestConfig): Promise<T> {
    return new Promise((resolve, reject) => {
      const authToken = this.getAuthToken()
      if (authToken === null) {
        // No auth token
        reject(401) // eslint-disable-line prefer-promise-reject-errors
        // Redirect
        this.loginRedirect('You must sign in to view this page.')
      }
      // Token valid, make request.
      // Pass on to returned promise.
      const authHeader = {
        Authorization: 'Bearer ' + authToken
      }
      // Add authorization header
      this.simpleReq(Object.assign(config, {
        headers: Object.assign(config.headers ? config.headers : {}, authHeader)
      })).then(data => resolve(data))
        .catch(code => {
          reject(code)
          if (code === 401 && this.redirectOn401()) {
            // Invalid auth, redirect
            this.loginRedirect('Login timed out. Sign in again to continue.')
          }
        })
    })
  }

  /**
   * Whether to redirect to the login page for a 401.
   */
  protected redirectOn401 (): boolean {
    return true
  }

  /**
   * @returns The base url to prefix any request with.
   */
  protected getBaseURL (): string {
    return getBaseURL()
  }

  /**
   * @returns An object encoded as form data.
   * @param obj The object to convert.
   */
  protected encodeAsForm (obj: { [key: string]: any }): FormData {
    const formData = new FormData()
    Object.keys(obj).forEach(key => {
      const val = obj[key]
      formData.append(key, val)
    })
    return formData
  }

  /**
   * Get the auth token for requests.
   */
  protected getAuthToken (): string | null {
    if (this.auth) {
      return this.auth
    }
    return localStorage.getItem(TOKEN_STORAGE_NAME)
  }

  /**
   * Override to define the behavior of the request.
   * @param opts The options (inputs) to the request.
   * @returns A promise yielding the result of the request.
   */
  abstract handle (opts: D): Promise<T>
}
