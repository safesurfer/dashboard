import Request from '@/requests/request'

export default class GetReferrerName extends Request<string, number> {
  /**
   * @returns The name of the referer given their ID.
   * @param opts The referer ID.
   */
  handle(opts: number): Promise<string> {
    return this.simpleReq({
      url: '/referrals/referers/' + encodeURIComponent(opts) + '/name',
      method: 'GET'
    })
  }
}
