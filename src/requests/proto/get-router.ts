import { getRouterProtoDomain } from "@/helpers/functions";
import Request from "../request";

export default class GetRouter extends Request<'Protocol Active', undefined> {
  handle(): Promise<"Protocol Active"> {
    return this.simpleReq({
      url: 'https://' + getRouterProtoDomain(),
      method: 'GET'
    })
  }

  getBaseURL (): string {
    return ''
  }
}
