import { getDohProtoDomain } from '@/helpers/functions'
import Request from '@/requests/request'

export default class GetDOH extends Request<'Protocol Active', undefined> {
  /**
   * @returns "Protocol Active" if the plain protocol is active.
   */
  handle (): Promise<'Protocol Active'> {
    return this.simpleReq({
      url: 'https://' + getDohProtoDomain(),
      method: 'GET'
    })
  }

  getBaseURL (): string {
    return ''
  }
}
