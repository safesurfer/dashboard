import Vue from 'vue'
import VueRouter from 'vue-router'
import { getLoginRoute, hasValidAuth } from '@/helpers/functions'
import Login from '@/views/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'AccountHome',
    component: () => import('@/views/AccountHome.vue'),
    meta: {
      title: 'Dashboard'
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      noAuthAccess: true,
      title: 'Log in'
    }
  },
  {
    path: '/account-settings',
    name: 'AccountSettings',
    component: () => import('@/views/AccountSettings.vue'),
    meta: {
      title: 'Account settings'
    }
  },
  {
    path: '/alerts',
    name: 'Alerts',
    component: () => import('@/views/Alerts.vue'),
    meta: {
      title: 'Alerts',
      help: [
        'The Alerts menu shows notable activity for protected devices, such as attempts to view blocked sites. It\'s good to keep in mind that some services (such as social media apps) are spread throughout the Internet. If something is being blocked in cases where the device user doesn\'t use the service in question, it\'s possible that it\'s being blocked correctly in the background (such as a shared social media post on an unrelated website).',
        'You can manage scheduled email alerts by clicking the Emails button.'
      ]
    }
  },
  {
    path: '/alert-settings',
    name: 'AlertSettings',
    component: () => import('@/views/AlertSettings.vue'),
    meta: {
      title: 'Alert emails',
      help: [
        'Deliver alerts to your inbox.',
        'We\'ve set you up with emails we think you\'ll find useful. You can turn them off, or customize with more specific settings.'
      ]
    }
  },
  {
    path: '/device',
    redirect: (to: any) => {
      return {
        name: 'Devices',
        query: {
          'device-id': to.query['device-id']
        }
      }
    }
  },
  {
    path: '/devices',
    name: 'Devices',
    component: () => import('@/views/Devices.vue'),
    meta: {
      title: 'Devices',
      help: [
        'A device will turn up in this list once you\'ve protected it. From there, you can edit its settings or view activity.',
        'To get started, click the New device option.' 
      ]
    }
  },
  {
    path: '/device-setup',
    name: 'DeviceSetup',
    component: () => import('@/views/DeviceSetup.vue'),
    meta: {
      title: 'Device setup',
    }
  },
  {
    path: '/blocking',
    name: 'Blocking',
    component: () => import('@/views/Blocking.vue'),
    meta: {
      title: 'Blocking & Screentime'
    }
  },
  {
    path: '/usage',
    name: 'Usage',
    component: () => import('@/views/Usage.vue'),
    meta: {
      title: 'Browsing history',
      help: [
        'Once you have protected a device with BRAND_NAME, you\'ll be able to review the websites it has attempted to access. Don\'t worry if you see lots of computer-related domains, or if you see requests happening overnight, which is normal.',
        'Social media posts can often be found within news articles on unrelated websites, so blocked attempts to access a social media service may not always be deliberate.'
      ]
    }
  },
  {
    path: '/vpn-alerts',
    name: 'VPNAlerts',
    component: () => import('@/views/VPNAlerts.vue'),
    meta: {
      title: 'VPN Alerts'
    }
  },
  {
    path: '/verify',
    name: 'Verify',
    component: () => import('@/views/Verify.vue'),
    meta: {
      noAuthAccess: true,
      title: 'Verify Account'
    }
  },
  {
    path: '/send-password-reset',
    redirect: {
      name: 'Login',
      query: {
        'auth-steps': JSON.stringify([0, 2]),
        'auth-step': '1'
      }
    }
  },
  {
    path: '/confirm-pass-reset',
    name: 'ConfirmPassReset',
    component: () => import('@/views/ConfirmPasswordReset.vue'),
    meta: {
      noAuthAccess: true,
      title: 'Password Reset'
    }
  },
  {
    path: '/create-account',
    name: 'CreateAccount',
    component: () => import('@/views/CreateAccount.vue'),
    meta: {
      noAuthAccess: true,
      title: 'Create Account'
    }
  },
  {
    path: '/clear-history',
    name: 'ClearHistory',
    component: () => import('@/views/ClearHistory.vue'),
    meta: {
      title: 'Clear history'
    }
  },
  {
    path: '/screentime/timers',
    redirect: {
      name: 'Blocking',
    }
  },
  {
    path: '/screentime/timetables',
    redirect: {
      name: 'Blocking',
    }
  },
  {
    path: '/self-service',
    name: 'SelfService',
    component: () => import('@/views/SelfService.vue'),
    meta: {
      hideHeaderFooter: true,
      noAuthAccess: true,
      title: 'My access'
    }
  },
  {
    path: '/screentime/stop-start-access',
    redirect: {
      name: 'Blocking',
    }
  },
  {
    path: '/twofactor/enrolment',
    name: 'TwofactorEnrolment',
    component: () => import('@/views/TwofactorEnrolment.vue'),
    meta: {
      title: 'Enable Two Factor Authorization'
    }
  },
  {
    path: '/device-registration',
    name: 'DeviceRegistration',
    component: () => import('@/views/DeviceRegistration.vue'),
    meta: {
      title: 'Device Registration',
      noAuthAccess: true,
      hideHeaderFooter: true
    }
  },
  {
    path: '/device-reauth',
    name: 'DeviceReauth',
    component: () => import('@/views/DeviceReauth.vue'),
    meta: {
      title: 'Device Reauth',
      noAuthAccess: true,
      hideHeaderFooter: true
    }
  },
  {
    path: '/router-registration',
    name: 'RouterRegistration',
    component: () => import('@/views/RouterRegistration.vue'),
    meta: {
      title: 'ROUTER_NAME Registration',
      hideHeaderFooter: true,
      noAuthAccess: true
    }
  },
  {
    path: '/security-alerts',
    name: 'SecurityAlerts',
    component: () => import('@/views/SecurityAlerts.vue'),
    meta: {
      title: 'Security Alerts'
    }
  },
  {
    path: '/plan-switch',
    name: 'PlanSwitch',
    component: () => import('@/views/PlanSwitch.vue'),
    meta: {
      title: 'Switch Plans'
    }
  },
  {
    path: '/stop-start-access',
    name: 'StopStartAccess',
    component: () => import('@/views/StopStartAccess.vue'),
    meta: {
      title: 'Screentime'
    }
  },
  {
    path: '/app-approval',
    redirect: (to: any) => {
      return {
        name: 'Blocking',
        query: {
          'device-id': to.query['device-id'],
          mac: '',
          tab: 'apps'
        }
      }
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes,
})

// Guard to redirect to login if we don't have valid auth
router.beforeEach((to, from, next) => {
  if (to && from && to.name && from.name && to.name !== from.name) {
    window.scrollTo(0, 0)
  }
  if (to.meta && !to.meta.noAuthAccess && !hasValidAuth()) {
    // Redirect to login
    next(getLoginRoute(to, to.fullPath))
  } else {
    // Proceed
    next()
  }
})

// Update page title
const BASE_TITLE = 'BRAND_NAME'
router.afterEach((to, from) => {
  Vue.nextTick(() => {
    // Construct the URL
    document.title = (to.meta && to.meta.title) ? to.meta.title + ' · ' + BASE_TITLE : BASE_TITLE
  })
})

export default router
