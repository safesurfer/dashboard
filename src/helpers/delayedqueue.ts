import { BlockingChanges } from "@/requests/blocking/post-blocking-changes";

export interface Combinable<T> {
  combine: (other: T) => T;
}

export class DelayedQueue<T extends Combinable<T>> {
  private delay: number
  private onReady: (queue: T) => Promise<any>
  private onReadyComplete: Promise<any> | null = null
  private onWaitingStateChange: (waiting: boolean) => void
  private timeoutId = 0
  private currQueue: T | null = null

  constructor(delay: number, onReady: (queue: T) => Promise<any>, onWaitingStateChange: (waiting: boolean) => void) {
    this.delay = delay
    this.onReady = onReady
    this.onWaitingStateChange = onWaitingStateChange
  }

  getQueue (): T | null {
    return this.currQueue
  }

  async enqueue(item: T) {
    if (this.onReadyComplete != null) {
      await this.onReadyComplete
    }
    this.onWaitingStateChange(true)
    if (this.currQueue === null) {
      this.currQueue = item
      this.timeoutId = setTimeout(() => {
        this.flush()
      }, this.delay)
    } else {
      clearTimeout(this.timeoutId)
      this.currQueue = this.currQueue.combine(item)
      this.timeoutId = setTimeout(() => {
        this.flush()
      }, this.delay)
    }
  }

  async flush() {
    const toSend = this.currQueue
    if (toSend === null) {
      return
    }
    clearTimeout(this.timeoutId)
    this.onReadyComplete = this.onReady(toSend)
    await this.onReadyComplete
    this.timeoutId = 0
    this.currQueue = null
    this.onReadyComplete = null
    this.onWaitingStateChange(false)
  }
}

export class Changes {
  changes: BlockingChanges

  constructor(changes: BlockingChanges) {
    this.changes = changes
  }

  combine(other: Changes): Changes {
    this.changes.changes = this.changes.changes.concat(other.changes.changes)
    return new Changes(this.changes)
  }
}
