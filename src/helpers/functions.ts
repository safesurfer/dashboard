import startCase from 'lodash/startCase'
import toLower from 'lodash/toLower'
import { TOKEN_STORAGE_NAME, BASE_URL_STAGING, CHARGEBEE_SITE_STAGING, BASE_URL_STAGING_IPV6 } from '@/helpers/constants'
import { Location, RawLocation, Route } from 'vue-router'
import PutDeviceIP from '@/requests/devices/put-device-ip'
import PutDeviceIp6 from '@/requests/devices-v2/put-device-ip6'
import GetDevicesList, { Device, DevicesList, GetDevicesListArgs } from '@/requests/devices-v2/get-devices-list'
import GetCategoryBlocking from '@/requests/blocking/get-category-blocking'
import cloneDeep from 'lodash/cloneDeep'
import assign from 'lodash/assign'
import isEqual from 'lodash/isEqual'
import { AppModel, AppsRuleset, BlockingModel, DeviceRef } from '@/requests/blocking/get-app-blocking'
import { CategoriesRuleset, CategoryModel } from '@/requests/blocking/patch-category-blocking-base-ruleset'
import { DomainModel } from '@/requests/blocking/post-blocking-changes'
import { DomainsRuleset } from '@/requests/blocking/get-blocking'

/**
 * @returns The IANA timezone string of the user, falling back to UTC.
 */
export function getTimezone (): string {
  let tzstring = 'UTC'
  if (window.Intl) {
    tzstring = Intl.DateTimeFormat().resolvedOptions().timeZone
  }
  return tzstring
}

/**
 * @returns A title case string, e.g. Title Case.
 * @param input String to convert.
 */
export function titleCase (input: string): string {
  return startCase(toLower(input))
}

export interface JWTClaims {
  id: string;
  deviceID: string;
  subStatus: string;
  nextBill: number;
  purchaseToken: string;
  planID: string;
  roles: string[];
  requestedExpiry: number;
  requestedDeviceID: string;
  requestedRoles: string[];
  exp: number;
}

/**
 * @returns The JWT claims object, given a JWT.
 * @param jwt The JWT string.
 */
export function parseClaims (jwt: string): JWTClaims {
  const parts = jwt.split('.')
  if (parts.length !== 3) {
    throw new Error('Invalid JWT')
  }
  const claimsPart = atob(parts[1])
  return JSON.parse(claimsPart)
}

/**
 * @returns Whether the web app has a valid auth token.
 */
export function hasValidAuth (): boolean {
  const jwt = localStorage.getItem(TOKEN_STORAGE_NAME)
  if (!jwt) {
    return false // No token stored
  }
  // Parse claims
  const claims = parseClaims(jwt)
  if (typeof claims.exp === 'number') {
    // Have valid expiry
    const timestampNow = Math.round(new Date().getTime() / 1000)
    return timestampNow < claims.exp // Whether is expired
  }
  return false // No valid expiry
}

/**
 * @param redirectFullPath The full path of the page to back to once logged in.
 * @returns A location that will go to the login page. 
 */
export function getLoginRoute (currRoute: Route, redirectFullPath: string): RawLocation {
  const query: {[key: string]: string | (string | null)[]} = {
    redirect: redirectFullPath,
    reason: 'You must log in to view this page.'
  }
  if (currRoute.query['ref']) {
    query['ref-follow'] = currRoute.query['ref']
  }
  if (currRoute.query['ref-follow']) {
    query['ref-follow'] = currRoute.query['ref-follow']
  }
  return {
    name: 'Login',
    query: query
  }
}

export function getTokenPrimaryRole (token: string): string | undefined {
  const claims = parseClaims(token)
  if (!claims.roles) {
    return undefined
  }
  if (!claims.roles.length) {
    return undefined
  }
  return claims.roles[0]
}

export function needsTwofactor (token: string): boolean {
  return getTokenPrimaryRole(token) === 'complete-twofactor'
}

/**
 * From https://webpushdemo.azurewebsites.net/
 * @returns A Uint8Array of a base64-encoded URL.
 * @param base64String The base64-encoded URL.
 */
export function urlBase64ToUint8Array(base64String: string) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
      .replace(/-/g, '+')
      .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i)  {
      outputArray[i] = rawData.charCodeAt(i);
  }

  return outputArray;
}

/**
 * @returns Whether we are in the development environment.
 */
export function isDevEnvironment(): boolean {
  return process.env.NODE_ENV === 'development'
}

/**
 * @returns The content of the meta tag with the name, or the empty string if it wasn't defined.
 * @param tagName The name of the meta tag.
 */
export function loadFromMetaTag(tagName: string): string {
  const el = document.head.querySelector('[name~=' + tagName + '][content]') as HTMLMetaElement
  if (!el) {
    return ''
  }
  return el.content
}

/**
 * @returns The content of a meta tag with the name, or a default value if the meta tag wasn't found or isn't set.
 * @param tagName The name of the meta tag.
 * @param defaultValue The value to use if the meta tag wasn't found. 
 * */ 
export function metaTagOrDefault(tagName: string, defaultValue: string): string {
  const content = loadFromMetaTag(tagName)
  if (!content || content.startsWith('{{')) {
    return defaultValue
  }
  return content
}

/**
 * @returns The base URL to use for the API.
 */
export function getBaseURL(): string {
  if (isDevEnvironment()) {
    return BASE_URL_STAGING
  }
  return loadFromMetaTag('apiURL')
}

/**
 * @returns The base URL to use for the IPV6-only API.
 */
export function getBaseURLIpv6(): string {
  if (isDevEnvironment()) {
    return BASE_URL_STAGING_IPV6
  }
  return loadFromMetaTag('apiIpv6URL')
}

/**
 * @returns The host used for DOT in this deployment.
 */
export function getDotHost(): string {
  return metaTagOrDefault('dotHost', '')
}

/**
 * @returns The DOH URL for this deployment.
 */
export function getDohURL(): string {
  return metaTagOrDefault('dohURL', '')
}

/**
 * @returns The chargebee site to use for payment.
 */
export function getChargebeeSite(): string {
  if (isDevEnvironment()) {
    return CHARGEBEE_SITE_STAGING
  }
  return metaTagOrDefault('chargebeeSite', '')
}

/**
 * @returns The URL to the categorizer.
 */
export function getCategorizerURL(): string {
  return metaTagOrDefault('categorizerURL', '')
}

/**
 * Log something to the console, only if we are in the dev environment.
 * @param msg 
 */
export function log(msg: any): void {
  if (isDevEnvironment()) {
    console.log(msg)
  }
}

/**
 * @returns A simple string description of the user's subscription status.
 * @param subStatus The subscription status from the JWT.
 */
export function getSubscriptionDescription(subStatus: string): string {
  if (subStatus === 'ACTIVE' || subStatus === 'COURTESY' || subStatus === 'NON_RENEWING' || subStatus === 'FUTURE') {
    return 'PLAN_NAME'
  } else if (subStatus === 'TRIAL' || subStatus === 'TRIAL_CANCELLED') {
    return 'Trial'
  } else {
    return 'Basic'
  }
}

/**
 * @returns The name of the role that routers will try to authenticate as,
 * since we must match this.
 */
export function getRouterRole(): string {
  return metaTagOrDefault('routerRole', 'router')
}

/**
 * @returns The page to open to show the router UI. 
 */
export function getRouterHost(): string {
  return metaTagOrDefault('routerHost', '')
}

export function getPlainProtoDomain(): string {
  return metaTagOrDefault('plainProtoDomain', '')
}

export function getDohProtoDomain(): string {
  return metaTagOrDefault('dohProtoDomain', '')
}

export function getDotProtoDomain(): string {
  return metaTagOrDefault('dotProtoDomain', '')
}

export function getDnscryptProtoDomain(): string {
  return metaTagOrDefault('dnscryptProtoDomain', '')
}

export function getRouterProtoDomain(): string {
  return metaTagOrDefault('routerProtoDomain', '')
}

export class IpResults {
  public ip4: LinkSourceIpResult | undefined
  public ip6: LinkSourceIpResult | undefined

  ipv6Available(): Boolean {
    return this.ip6 === LinkSourceIpResult.Ok
  }
}

export enum LinkSourceIpResult {
  Ok, Conflict, Unknown, NotRelevant
}

export function linkSourceIp(device: string): Promise<IpResults> {
  const result = new IpResults()
  return new Promise((resolve, reject) => {
    new PutDeviceIP().handle(device).then(() => {
      result.ip4 = LinkSourceIpResult.Ok
    }).catch(err => {
      if (err) {
        if (err == 400) {
          result.ip4 = LinkSourceIpResult.Conflict
        } else {
          result.ip4 = LinkSourceIpResult.Unknown
        }
      } else {
        result.ip4 = LinkSourceIpResult.Unknown
      }
    }).then(() => {
      new PutDeviceIp6().handle(device).then(() => {
        result.ip6 = LinkSourceIpResult.Ok
      }).catch(err => {
        if (err) {
          if (err == 400) {
            result.ip6 = LinkSourceIpResult.Conflict
          } else {
            result.ip6 = LinkSourceIpResult.Unknown
          }
        } else {
          result.ip6 = LinkSourceIpResult.NotRelevant
        }
      }).then(() => resolve(result))
    })
  })
}

/**
 * @returns Whether the return URL for the router is allowed. This URL will receive
 * the router's HMAC key and the user's email, so it's important that we don't
 * leak to unauthorized domains.
 * @param ret The proposed return url.
 */
export function routerReturnURLAllowed(ret: string): boolean {
  const allowedPrefixes: string[] = isDevEnvironment() ?
    [ 'http://192.168.8.1', 'http://localhost:' ] :
    JSON.parse(metaTagOrDefault('routerHosts', '[]'))
  for (let i = 0; i < allowedPrefixes.length; i++) {
    if (ret.startsWith(allowedPrefixes[i])) {
      return true
    }
  }
  return false
}

export interface PlainDnsServers {
  v4: string[]
  v6: string[]
}

// Use a combination of Cloudflare DNS and traffic manager to roughly determine the
// best-performing GCP servers according to the traffic manager config.
// The actual IPs returned by the lookup are not relevant, although they do work,
// the global one is used for Azure in all cases anyway.
export function getBestPlainDnsServers(): Promise<PlainDnsServers> {
    return new Promise((resolve, reject) => {
        resolve({
          v4: ['0.0.0.0', '0.0.0.0'],
          v6: ['::', '::']
        })
    })
}

export function getBlockedBaseCategories(deviceId: string): Promise<number[]> {
  return new Promise<number[]>((resolve, reject) =>
    new GetCategoryBlocking().handle({
      resolve: true,
      'device-id': deviceId,
      'exclude-screentime-rules': true
    }).then(categories => {
      if (!categories.spec || !categories.spec.resolvedBlockingModel) {
        resolve([])
        return
      }
      const blocked: number[] = []
      Object.keys(categories.spec.resolvedBlockingModel).forEach(category => {
        const categoryId = parseInt(category)
        const resolved = categories.spec.resolvedBlockingModel[categoryId]
        if (resolved.blocked) {
          blocked.push(categoryId)
        }
      })
      resolve(blocked)
    }).catch(err => reject(err))
  )
}

export function getDeviceIcon (device: Device): string | undefined {
  switch (device.type) {
    case 'router':
      return 'router-wireless'
    case 'router-client':
      return 'devices'
    case 'dns':
      return 'dns'
    case 'android':
      return 'android'
    case 'ios':
      return 'apple'
    case 'windows':
      return 'microsoft'
    default:
      return undefined
  }
}

let devicesCache: DevicesList | null = null
let devicesCacheUpdated = 0

export interface GetDevicesOptions {
  invalidate?: boolean;
  auth?: string;
  requestArgs?: GetDevicesListArgs;
}

export function invalidateDeviceCache() {
  devicesCache = null
}

export function getDevices(options?: GetDevicesOptions): Promise<DevicesList> {
  if (!options) {
    options = {}
  }
  if (options.invalidate) {
    invalidateDeviceCache()
  }
  if (devicesCache === null || Math.floor(Date.now() / 1000) - devicesCacheUpdated > 5*60_000) {
    return new Promise((resolve, reject) => {
      const req = new GetDevicesList()
      if (options && options.auth) {
        req.setAuth(options.auth)
      }
      req.handle(options && options.requestArgs ? options.requestArgs : {}).then(resp => {
        devicesCache = resp.spec
        devicesCacheUpdated = Math.floor(Date.now() / 1000)
        resolve(cloneDeep(resp.spec)) 
      }).catch(err => reject(err))
    })
  } else {
    const cachedVal = devicesCache
    return new Promise((resolve, _) => resolve(cloneDeep(cachedVal)))
  }
}

export interface DeviceSelection {
  id: string;
  mac: string;
}

export function deviceSelectionFromRoute(route: Route): DeviceSelection | undefined {
  const s: DeviceSelection = {
    id: '',
    mac: ''
  }
  const qId = route.query['device-id']
  if (typeof qId === 'string') {
    if (qId === '-') {
      return undefined
    }
    s.id = qId
  }
  const qMac = route.query['mac']
  if (typeof qMac === 'string') {
    s.mac = qMac
  }
  return s
}

export function deviceSelectionToRoute(currRoute: Route, selection: DeviceSelection | undefined): RawLocation {
  if (!currRoute.name) {
    throw new Error('route not named!')
  }
  return {
    name: currRoute.name,
    query: assign(cloneDeep(currRoute.query), selection ? {
      'device-id': selection.id,
      'mac': selection.mac
    } : {'device-id': '-'})
  }
}

export function getDayString(day: string): string {
  switch (day) {
    case "0":
      return "Monday"
    case "1":
      return "Tuesday"
    case "2":
      return "Wednesday"
    case "3":
      return "Thursday"
    case "4":
      return "Friday"
    case "5":
      return "Saturday"
    case "6":
      return "Sunday"
    case "weekday":
      return 'Weekdays'
    case "weekend":
      return 'Weekend days'
    default:
      return 'All days'
  }
}

export function formatTimerDuration(day: string, maxTimeSecs: number): string {
  let duration = ''
  // Add amount of hours, if any
  const hours = Math.floor(maxTimeSecs / 60 / 60)
  if (hours > 0) {
    duration += hours + 'h'
  }
  const minutes = Math.floor((maxTimeSecs - Math.floor(maxTimeSecs / 60 / 60) * 60 * 60) / 60)
  // Add amount of minutes, if any
  if (minutes > 0) {
    duration += minutes + 'm'
  }
  // Add the day
  const dayStr = getDayString(day)
  return duration + ' on ' + dayStr
}

const APPLICABLE_DAYS: {[day: number]: string[]} = {
  0: ["0", "weekday", "*"],
  1: ["1", "weekday", "*"],
  2: ["2", "weekday", "*"],
  3: ["3", "weekday", "*"],
  4: ["4", "weekday", "*"],
  5: ["5", "weekend", "*"],
  6: ["6", "weekend", "*"],
}

export interface TimetableTimes {
  everActive: boolean;
  alwaysActive: boolean;
  isAfter: boolean;
  sentence: string;
}

export function formatTimetableTimes (activeTimes: {[day: string]: boolean[]}): TimetableTimes {
  const blocked = (t: [number, number]) => { // day, quarter-number
      return APPLICABLE_DAYS[t[0]].some(it => activeTimes[it][t[1]])
  }
  // The approach here is to loop through all possible quarters of an hour of the week and
  // add a subsection for each blocked time period. Based on that, try different strategies
  // to get the shortest string.
  // To get the best possible format, attempt to start at the beginning of a day that
  // does not begin by being active.
  let startDay = 0
  for (let day = 5; day <= 11; day++) {
      if (!blocked([day%7, 0])) {
          startDay = day%7
          break
      }
  }
  const activePeriods: [[number, number], [number, number]][] = []
  let activePeriodStart: [number, number] | null = null
  for (let dayWrapped = startDay; dayWrapped < startDay+7; dayWrapped++) {
    const day = dayWrapped % 7
    for (let i = 0; i < 96; i++) {
      const t: [number, number] = [day, i]
      const isBlocked = blocked(t)
      if (isBlocked && activePeriodStart === null) {
        // Mark this as the start
        activePeriodStart = t
      } else if (!isBlocked && activePeriodStart !== null) {
        // Mark this as the end
        // Does it loop around to the beginning?
        if (activePeriods.length && t === activePeriods[0][1]) {
          activePeriods[0] = [activePeriodStart, t]
          activePeriodStart = null
          break
        } else {
          activePeriods.push([activePeriodStart, t])
          activePeriodStart = null
        }
      }
    }
  }
  if (activePeriodStart != null) {
    // Never finished the Last seen period. Finish it, unless it already exists.
    const toAdd: [[number, number], [number, number]] = [activePeriodStart, [startDay, 0]]
    if (!activePeriods.filter(it => it[0] == toAdd[0]).length) {
      activePeriods.push(toAdd)
    }
  }
  if (!activePeriods.length) {
    return {
      everActive: false,
      alwaysActive: false,
      isAfter: false,
      sentence: ''
    }
  }
  // Special case - is the timetable always active?
  if (activePeriods.length === 1 &&
    isEqual(activePeriods[0][0], [startDay, 0]) &&
    isEqual(activePeriods[0][1], [startDay, 0])) {
    return {
      everActive: true,
      alwaysActive: true,
      isAfter: false,
      sentence: ""
    }
  }
  // Regular case - list the active intervals, compressing identical intervals on successive
  // days into one. To ensure we get all the compression we can, start at the first day
  // transition we can find.
  let startIdx = 0
  if (activePeriods.length > 1) {
    for (let i = 0; i < activePeriods.length; i++) {
      const currPeriod = activePeriods[i]
      if (currPeriod[0][0] !== currPeriod[1][0]) {
        continue
      }
      const nextI = i === activePeriods.length-1 ? 0 : i+1
      const nextPeriod = activePeriods[nextI]
      if (nextPeriod[0][0] != nextPeriod[1][0]) {
        continue
      }
      if (currPeriod[0][0] != nextPeriod[0][0]) {
        startIdx = i
        break
      }
    }
  }
  // Now we'll use two different strategies to find the shortest way to express this
  // timetable. The shortest will be used.
  const adjacentResult = compressAdjacentIntervals(activePeriods, startIdx)
  const adjacentDayResult = compressAdjacentDays(activePeriods, startIdx)
  if (adjacentDayResult && adjacentDayResult.sentence.length < adjacentResult.sentence.length) {
    return adjacentDayResult
  } else {
    return adjacentResult
  }
}

function formatHourAndQuarter(hour: number, quarter: number): String {
  let middlePortion = ''
  const minutes = 15*quarter
  if (minutes === 60) {
    hour += 1
  } else if (minutes !== 0) {
    middlePortion += ':' + minutes
  }
  if (hour == 0) {
    return '12' + middlePortion + 'am'
  } else if (hour < 12) {
    return hour + middlePortion + 'am'
  } else if (hour == 12) {
    return '12' + middlePortion + 'pm'
  } else if (hour == 24) {
    return '12' + middlePortion + 'am'
  } else {
    return (hour-12) + middlePortion + 'pm'
  }
}

function fmtQuartNum(quarterNum: number): String {
  const hour = Math.floor(quarterNum / 4)
  const quarter = quarterNum % 4
  return formatHourAndQuarter(hour, quarter)
}

function compressAdjacentDays(activePeriods: [[number, number], [number, number]][], startIdx: number): TimetableTimes | null {
  // Group by day
  let byDay: [[number, number], [number, number]][][] = []
  for (let iWrapped = startIdx; iWrapped < startIdx+activePeriods.length; iWrapped++) {
    const i = iWrapped % activePeriods.length
    const pair = activePeriods[i]
    const start = pair[0]
    const end = pair[1]
    const isSameDay = start[0] === end[0]
    const isAdjacentDays = end[0]-start[0] === 1 || end[0]-start[0] === -6
    const endIsStartOfNextDay = end[1] === 0
    const intervals: [[number, number], [number, number]][] | null = (isSameDay || (isAdjacentDays && endIsStartOfNextDay)) ? [pair] :
      (isAdjacentDays ? [[[start[0], start[1]], [end[0], 0]], [[end[0], 0], [end[0], end[1]]]] : null)
    if (intervals === null) {
      return null
    }
    intervals.forEach(interval => {
      let existingList = byDay.find(it => it[0][0][0] === interval[0][0])
      if (!existingList) {
        const newList: [[number, number], [number, number]][] = []
        byDay.push(newList)
        existingList = newList
      }
      existingList.push(interval)
    })
  }
  // Sort the time periods by start time within each day
  byDay = byDay.map(it => it.sort((i1, i2) => i1[0][1] - i2[0][1]))
  // As a special case, try to match the "After" pattern
  if (byDay.length > 1) {
    // Whether each day has an "After"/"Until" part, and if so, the quarter number.
    let canMatchAfter = true
    let startIdx = -1
    const afterUntilParts = byDay.map((it, i) => {
      if (it.length > 2) {
        canMatchAfter = false
        return null
      } else if (it.length == 2) {
        if (it[0][0][1] === 0 && it[1][1][1] === 0) {
          return [it[0][1][1], it[1][0][1]]
        } else {
          canMatchAfter = false
          return null
        }
      } else if (it[0][0][1] === 0) {
        return [it[0][1][1], null]
      } else if (it[0][1][1] === 0) {
        startIdx = i
        return [null, it[0][0][1]]
      } else {
        canMatchAfter = false
        return null
      }
    })
    if (canMatchAfter) {
      // Ensure every after/until matches.
      let validAfter: number | null = null
      let validUntil: number | null = null
      if (afterUntilParts.every(it => {
        if (it === null) {
          return true
        } else {
          const [until, after] = it
          if (validAfter === null) {
            validAfter = after
          }
          if (validUntil === null) {
            validUntil = until
          }
          return (until === null || until === validUntil) && (after === null || after === validAfter)
        }
      }) && validAfter !== null && validUntil !== null) {
        if (startIdx === -1 && afterUntilParts.length === 7) {
          // Goes all the way around - every day.
          return {
            everActive: true,
            alwaysActive: false,
            isAfter: true,
            sentence: fmtQuartNum(validAfter) + ' until ' + fmtQuartNum(validUntil) + ' every day'
          }
        } else if (startIdx !== -1) {
          // Ensure the days with a valid after/until are continuous.
          const partTypeOf = (part: [number | null, number | null]) => {
            if (part[1] !== null && part[0] === null) {
              return 1
            } else if (part[0] !== null && part[1] === null) {
              return -1
            } else {
              return 0
            }
          }
          let isContinuous = true
          let numStartTransitions = 0
          let numMiddleTransitions = 0
          let numEndTransitions = 0
          let endIdx = -1
          for (let iWrapped = startIdx; iWrapped < startIdx+afterUntilParts.length; iWrapped++) {
            const i = iWrapped % afterUntilParts.length
            const nextI = (iWrapped + 1) % afterUntilParts.length
            const partType = partTypeOf(afterUntilParts[i] as [number | null, number | null])
            const nextPartType = partTypeOf(afterUntilParts[nextI] as [number | null, number | null])
            const currDay = byDay[i][0][0][0]
            const nextDay = byDay[nextI][0][0][0]
            const isContinuousDay = nextDay-currDay === 1 || nextDay-currDay === -6
            if (partType === 1) {
              // Start of the pattern. Only a full day or end day are allowed to
              // occur next. A start transition can only occur once and must
              // occur on continuous days.
              if (nextPartType === 1 || !isContinuousDay) {
                isContinuous = false
                break
              } else {
                numStartTransitions++
                if (numStartTransitions > 1) {
                  isContinuous = false
                  break
                }
              }
            } else if (partType === 0) {
              // Middle of the pattern. Only a full day or end day are allowed
              // to occur next and must occur on continuous days.
              // A full -> end transition can only occur once.
              if (nextPartType === 1 || !isContinuousDay) {
                isContinuous = false
                break
              } else if (nextPartType === -1) {
                numMiddleTransitions++
                endIdx = i
                if (numMiddleTransitions > 1) {
                  isContinuous = false
                  break
                }
              }
            } else {
              // End of the pattern. Only a start day is allowed
              // to occur next. An end transition can only occur once.
              // The start day does not have to occur on a continuous day
              // because there may be empty day(s) in between.
              if (nextPartType === 0 || nextPartType === -1) {
                isContinuous = false
                break
              } else {
                numEndTransitions++
                if (numEndTransitions > 1) {
                  isContinuous = false
                  break
                }
              }
            }
          }
          if (isContinuous && numStartTransitions === 1 && numEndTransitions === 1) {
            const startDay = byDay[startIdx][0][0][0]
            if (endIdx === -1) {
              return {
                everActive: true,
                alwaysActive: false,
                isAfter: true,
                sentence: fmtQuartNum(validAfter) + ' ' + getDayString(startDay + '') + ' until ' + fmtQuartNum(validUntil) + ' the next day'
              }
            } else {
              const endDay = byDay[endIdx][0][0][0]
              return {
                everActive: true,
                alwaysActive: false,
                isAfter: true,
                sentence: fmtQuartNum(validAfter) + ' ' + getDayString(startDay + '') + '-' + getDayString(endDay + '') + ' until ' + fmtQuartNum(validUntil) + ' the next day'
              }
            }
          }
        }
      }
    }
  }
  // Function to get whether the intervals on day i are identical to the next day, wrapping around
  const getIntervalsMatch = (i: number) => {
    const intervals = byDay[i]
    const nextIntervals = byDay[(i+1) % byDay.length]
    return (nextIntervals[0][0][0]-intervals[0][0][0] === 1 || nextIntervals[0][0][0]-intervals[0][0][0] === -6) && // Is adjacent days
      intervals.length === nextIntervals.length && // Could match based on size
      intervals.every((pair, i) => { // Matches based on intervals
        const other = nextIntervals[i]
        return other[0][1] === pair[0][1] && other[1][1] === pair[1][1]
      })
  }
  // Find the day to start at that will give us the best result
  let startDayIdx = 0
  if (byDay.length > 1) {
    for (let i = 0; i < byDay.length; i++) {
      if (!getIntervalsMatch(i)) {
        startDayIdx = (i+1) % byDay.length
        break
      }
    }
  }

  let numIntervals = 0
  let sentence = ""
  let identicalIntervalStart = -1
  for (let iWrapped = startDayIdx; iWrapped < startDayIdx+byDay.length; iWrapped++) {
    const i = iWrapped % byDay.length
    const intervals = byDay[i]
    const intervalsMatch = iWrapped < startDayIdx+byDay.length-1 && getIntervalsMatch(i)
    if (intervalsMatch && identicalIntervalStart === -1) {
      // Start the interval here. A further iteration will add to the sentence.
      identicalIntervalStart = i
      continue
    } else if (!intervalsMatch && identicalIntervalStart !== -1) {
      // Interval ends here - combine the days together
      // Did we go all the way around?
      const startDay = byDay[identicalIntervalStart][0][0][0]
      const currDay = intervals[0][0][0]
      if (startDay-currDay === 1 || startDay-currDay === -6) {
        numIntervals = 1
        sentence = ""
        intervals.forEach((it, i) => {
          if (i > 0) {
            sentence += " "
          }
          sentence += fmtQuartNum(it[0][1]) + '-' + fmtQuartNum(it[1][1])
        })
        sentence += " every day"
      } else {
        if (numIntervals > 0) {
          sentence += ", "
        }
        intervals.forEach((it, i) => {
          if (i > 0) {
            sentence += " "
          }
          sentence += fmtQuartNum(it[0][1]) + '-' + fmtQuartNum(it[1][1])
        })
        sentence += ' ' + getDayString(startDay + '') + '-' + getDayString(currDay + '')
      }
      identicalIntervalStart = -1
      numIntervals++
      continue
    } else if (intervalsMatch) {
      // In the middle of an interval. It doesn't start here and it doesn't end here.
      continue
    }
    if (numIntervals > 0) {
      sentence += ", "
    }
    intervals.forEach((it, i) => {
      if (i > 0) {
        sentence += " "
      }
      sentence += fmtQuartNum(it[0][1]) + '-' + fmtQuartNum(it[1][1])
    })
    sentence += ' ' + getDayString(intervals[0][0][0] + '')
    numIntervals++
  }
  sentence += "."
  return {
    everActive: true,
    alwaysActive: false,
    isAfter: false,
    sentence: sentence
  }
}

function compressAdjacentIntervals(activePeriods: [[number, number], [number, number]][], startIdx: number): TimetableTimes {
  let numIntervals = 0
  let sentence = ""
  let identicalIntervalStart = -1
  for (let iWrapped = startIdx; iWrapped < startIdx+activePeriods.length; iWrapped++) {
    const i = iWrapped % activePeriods.length
    const pair = activePeriods[i]
    const start = pair[0]
    const end = pair[1]
    let intervalMatches = false
    if (i < activePeriods.length-1) {
      const nextStart = activePeriods[i+1][0]
      const nextEnd = activePeriods[i+1][1]
      intervalMatches = start[1] === nextStart[1] &&
        end[1] === nextEnd[1] &&
        start[0] === end[0] &&
        nextStart[0] === nextEnd[0] &&
        start[0] !== nextStart[0]
    }
    if (intervalMatches && identicalIntervalStart === -1) {
      // Start the interval here. A further iteration will add to the sentence.
      identicalIntervalStart = start[0]
      continue
    } else if (!intervalMatches && identicalIntervalStart !== -1) {
      // Interval ends here - combine the days together
      // Did we go all the way around?
      if (identicalIntervalStart === end[0]) {
        numIntervals = 1
        sentence = fmtQuartNum(start[1]) + '-' + fmtQuartNum(end[1]) + ' every day'
      } else {
        if (numIntervals > 0) {
          sentence += ", "
        }
        sentence += fmtQuartNum(start[1]) + '-' + fmtQuartNum(end[1]) + ' ' + getDayString(identicalIntervalStart + '') + '-' + getDayString(end[0] + '')
      }
      identicalIntervalStart = -1
      numIntervals++
      continue
    } else if (intervalMatches) {
      // In the middle of an interval. It doesn't start here and it doesn't end here.
      continue
    }
    if (numIntervals > 0) {
      sentence += ", "
    }
    sentence += getDayString(start[0] + '') + ' ' + fmtQuartNum(start[1]) + '-' + getDayString(end[0] + '') + ' ' + fmtQuartNum(end[1])
    numIntervals++
  }
  if (identicalIntervalStart !== -1) {
    // Never got to finish our interval. Make it end here, unless it actually went
    // through all the days.
    const lastIdx = (startIdx+activePeriods.length-1) % activePeriods.length
    const lastPeriod = activePeriods[lastIdx]
    const start = lastPeriod[0]
    const end = lastPeriod[1]
    if (identicalIntervalStart === end[0]) {
      // Went through all days
      sentence = fmtQuartNum(start[1]) + '-' + fmtQuartNum(end[1]) + ' every day'
    } else {
      // Just stop here
      if (numIntervals > 0) {
        sentence += ", "
      }
      sentence += fmtQuartNum(start[1]) + '-' + fmtQuartNum(end[1]) + ' ' + getDayString(identicalIntervalStart + '') + '-' + getDayString(end[0] + '')
    }
  }
  sentence += "."
  return {
    everActive: true,
    alwaysActive: false,
    isAfter: false,
    sentence: sentence
  }
}

export function listToString(list: string[], cutoff: number): string {
  const amountCutOff = list.length > cutoff ? list.length - cutoff : 0
  const joinStopIdx = amountCutOff > 0 ? list.length - 1 : list.length - 2
  let s = ''
  for (let i = 0; i < list.length; i++) {
    const item = list[i]
    if (i >= cutoff) {
      break
    }
    s += item
    if (i === joinStopIdx) {
      s += ' and '
    } else if (i < joinStopIdx) {
      s += ', '
    }
  }
  if (amountCutOff > 0) {
    s += 'and ' + amountCutOff + ' more'
  }
  return s
}

export interface AppDisplay {
  screentime: boolean
  model: AppModel
  timers: number[]
  timetables: number[]
  device: DeviceRef
}

export function getAppDisplayForModel (m: BlockingModel<AppsRuleset>, app: string, screentime: boolean | undefined, tempScreentimeApps: {[app: string]: boolean}): AppDisplay {
  const toCheck = [ m ]
  while (toCheck.length) {
    const model = toCheck.shift() as BlockingModel<AppsRuleset>
    const display: AppDisplay = { screentime: false, model: { blocked: false }, timers: [], timetables: [], device: model.deviceRef }
    if (screentime) {
      if (model.timers) {
        model.timers.forEach(timer => {
          if (timer.ruleset && timer.ruleset.apps && timer.ruleset.apps[app]) {
            display.timers.push(timer.id)
          }
        })
      }
      if (model.timetables) {
        model.timetables.forEach(timetable => {
          if (timetable.ruleset && timetable.ruleset.apps && timetable.ruleset.apps[app]) {
            display.timetables.push(timetable.id)
          }
        })
      }
    }
    if (display.timers.length || display.timetables.length || tempScreentimeApps[app]) {
      display.screentime = true
      return display
    } else if (model.baseRuleset && model.baseRuleset.apps && model.baseRuleset.apps[app]) {
      display.model = (model.baseRuleset.apps[app] as AppModel)
      return display
    } else if (model.parents) {
      // Nothing found here, try parents
      for (const parent of model.parents) {
        toCheck.push(parent)
      }
    }
  }
  return { screentime: false, model: { blocked: false }, timers: [], timetables: [], device: { accountId: '', deviceId: '', mac: '' } }
}

export interface CategoryDisplay {
  screentime: boolean
  model: CategoryModel
  timers: number[]
  timetables: number[]
  device: DeviceRef
}

export function getCategoryDisplayForModel (m: BlockingModel<CategoriesRuleset>, categoryId: number, screentime: boolean | undefined, tempScreentimeCategories: {[id: number]: boolean}): CategoryDisplay {
  const toCheck = [ m ]
  while (toCheck.length) {
    const model = toCheck.shift() as BlockingModel<CategoriesRuleset>
    const display: CategoryDisplay = { screentime: false, model: {}, timers: [], timetables: [], device: model.deviceRef }
    if (screentime) {
      if (model.timers) {
        model.timers.forEach(timer => {
          if (timer.ruleset && timer.ruleset.categories && timer.ruleset.categories[categoryId]) {
            display.timers.push(timer.id)
          }
        })
      }
      if (model.timetables) {
        model.timetables.forEach(timetable => {
          if (timetable.ruleset && timetable.ruleset.categories && timetable.ruleset.categories[categoryId]) {
            display.timetables.push(timetable.id)
          }
        })
      }
    }
    if (display.timers.length || display.timetables.length || (screentime && tempScreentimeCategories[categoryId])) {
      display.screentime = true
      return display
    } else if (model.baseRuleset && model.baseRuleset.categories && model.baseRuleset.categories[categoryId]) {
      display.model = (model.baseRuleset.categories[categoryId] as CategoryModel)
      return display
    } else if (model.parents) {
      // Nothing found here, try parents
      for (const parent of model.parents) {
        toCheck.push(parent)
      }
    }
  }
  return { screentime: false, model: { }, timers: [], timetables: [], device: { accountId: '', deviceId: '', mac: '' } }
}

export interface DomainDisplay {
  screentime: boolean
  model: DomainModel
  timers: number[]
  timetables: number[]
  device: DeviceRef
}

export function getDomainDisplayForModel (m: BlockingModel<DomainsRuleset>, domain: string, screentime: boolean | undefined, tempScreentimeDomains: {[domain: string]: boolean}): DomainDisplay {
  const toCheck = [ m ]
  while (toCheck.length) {
    const model = toCheck.shift() as BlockingModel<DomainsRuleset>
    const display: DomainDisplay = { screentime: false, model: { blocked: false }, timers: [], timetables: [], device: model.deviceRef }
    if (screentime) {
      if (model.timers) {
        model.timers.forEach(timer => {
          if (timer.ruleset && timer.ruleset.domains && timer.ruleset.domains[domain]) {
            display.timers.push(timer.id)
          }
        })
      }
      if (model.timetables) {
        model.timetables.forEach(timetable => {
          if (timetable.ruleset && timetable.ruleset.domains && timetable.ruleset.domains[domain]) {
            display.timetables.push(timetable.id)
          }
        })
      }
    }
    if (display.timers.length || display.timetables.length || tempScreentimeDomains[domain]) {
      display.screentime = true
      return display
    } else if (model.baseRuleset && model.baseRuleset.domains && model.baseRuleset.domains[domain]) {
      display.model = (model.baseRuleset.domains[domain] as DomainModel)
      return display
    } else if (model.parents) {
      for (const parent of model.parents) {
        toCheck.push(parent)
      }
    }
  }
  return { screentime: false, model: { blocked: false }, timers: [], timetables: [], device: { accountId: '', deviceId: '', mac: '' } }
}

export function getDeviceType (device: Device | undefined): string {
  if (!device) {
    return ''
  }
  if (device.type === 'group') {
    return 'User'
  } else if (device.type === 'router') {
    return 'ROUTER_NAME'
  } else if (device.type === 'router-client') {
    return 'ROUTER_NAME Client Device'
  } else if (device.type === 'dns') {
    return 'Connected via DNS'
  } else if (device.type === 'android') {
    return 'Android Device'
  } else if (device.type === 'ios') {
    return 'Apple Device'
  } else if (device.type === 'windows') {
    return 'Windows Device'
  }
  return ''
}
