export const TOKEN_STORAGE_NAME = 'my-ss-token'
export const EMAIL_STORAGE_NAME = 'my-ss-user'
export const DISMISSED_ANNOUNCEMENTS_STORAGE_NAME = 'my-ss-dismissed-announcements'
export const BASE_URL_STAGING = ''
export const BASE_URL_STAGING_IPV6 = ''
export const CHARGEBEE_SITE_STAGING = ''

/**
 * Describes a way of setting up BRAND_NAME.
 */
export interface SetupOption {
  title: string;
  icon: string;
  description?: string;
  /**
   * Either one of below must be specified.
   * Route is a Vue route object.
   */
  children?: SetupOption[];
  route?: any;
  link?: string;
  tags?: SetupOptionTag[];
  componentName?: string;
  userSelect?: boolean;
}
  
export interface SetupOptionTag {
  type: string;
  text: string;
}

export function getAllSetupOptions(): SetupOption[] {
  return [
    {
      title: 'Advanced',
      icon: 'wrench',
      description: 'Set up any kind of device/network that can change its DNS.',
      children: [
        {
          title: 'Advanced',
          icon: '',
          componentName: 'advanced-dns-setup',
          userSelect: true
        }
      ]
    }
  ]
}
