import 'array-flat-polyfill'

export function polyFill () {
  if (!String.prototype.includes) {
    String.prototype.includes = function (search: any, start) { // eslint-disable-line  
      if (search instanceof RegExp) {
        throw TypeError('first argument must not be a RegExp')
      }
      if (start === undefined) { start = 0 }
      return this.indexOf(search, start) !== -1
    }
  }

  if (!Array.prototype.includes) {
    Array.prototype.includes = function (searchElement) {  // eslint-disable-line
      return this.indexOf(searchElement) !== -1
    }
  }

  if (!String.prototype.startsWith) {
    Object.defineProperty(String.prototype, 'startsWith', {
      value: function(search: any, rawPos: any) {
        const pos = rawPos > 0 ? rawPos|0 : 0;
        return this.substring(pos, pos + search.length) === search;
      }
    });
  }
}
