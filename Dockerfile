FROM node:15.9.0-alpine3.13 AS ui
WORKDIR /app
COPY src /app/src
COPY public /app/public
COPY *.js *.json /app/
RUN npm i
RUN npm run build

FROM registry.gitlab.com/safesurfer/go-http-server:1.4.0
WORKDIR /app

# Enable history mode and templating
ENV APP_VUEJS_HISTORY_MODE true
ENV APP_TEMPLATE_MAP_PATH /app/templateMap.yaml
COPY config/templateMap.yaml .

# Enable custom headers
ENV APP_HEADER_SET_ENABLE true
ENV APP_HEADER_MAP_PATH /app/headers.yaml
COPY config/headers.yaml .

# Add the files
ENV APP_SERVE_FOLDER /app/dist
COPY --from=ui /app/dist dist
