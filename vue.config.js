module.exports = {
  lintOnSave: false,
  productionSourceMap: false,
  pwa: {
    name: 'BRAND_NAME - My Account',
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: './src/sw.js'
    },
    themeColor: '#1468B1'
  },
  // Enable the below for HTTPS
  devServer: {
    host: '0.0.0.0',
    port: 8080, // CHANGE YOUR PORT HERE!
  },
}